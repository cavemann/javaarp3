package exam.casting;

import org.apache.log4j.Logger;

public class CastingDemo {
    private static final Logger LOGGER = Logger.getLogger(CastingDemo.class);

    public static void main(String[] args) {
        Animal animal1 = new Horse("Konik");
        Animal animal2 = (Animal) new Horse("Zrebak");

        animal1.move();
        LOGGER.info("=================");
        animal2.move();
        LOGGER.info("==============================");
        ((Animal) animal2).move();
        LOGGER.info("Jak widać wyżej, rzutowanie klas przy wołaniu metod jest ignorowane");
        LOGGER.info("==============================");
        ((Horse) animal1).originalMove();
        LOGGER.info("Tak zadziała, mówimy Javie żeby potraktowała animal1 jako obiekt Horse a nie Animal");
        LOGGER.info("==============================");
        LOGGER.info("==============================");

        Riding riding1 = new Horse("Kary");
        Riding riding2 = (Riding) new Horse("Gniady");
        LOGGER.info(riding1.getRidingDetails());
        LOGGER.info(riding2.getRidingDetails());
        Riding riding3 = new Pony();
        Riding riding4 = (Riding) new Pony();
        LOGGER.info(riding3.getRidingDetails());
        LOGGER.info(riding4.getRidingDetails());
        LOGGER.info(((Riding) riding4).getRidingDetails());


        LOGGER.info("==============================");
        LOGGER.info(riding3.numberOfRides());
        LOGGER.info(riding4.numberOfRides());
    }
}
