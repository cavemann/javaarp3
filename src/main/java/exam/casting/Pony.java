package exam.casting;

public class Pony implements Riding {

    @Override
    public String getRidingDetails() {
        return "Raz w siodle, raz na oklep";
    }

    @Override
    public int numberOfRides() {
        return 10;
    }
}
