package exam.casting;

import org.apache.log4j.Logger;

public class Horse extends Animal implements Riding {

    private static final Logger LOGGER = Logger.getLogger(Horse.class);

    public Horse(String name) {
        super(name); //wywołuje konstruktora nadklasy (Animal)
    }

    @Override
    public void move() {
        LOGGER.info("Horse is running");
    }

    public void originalMove() {
        super.move(); // wywołanie metody move() z nadklasy - czyli wołamy oryginał a nie to
        //co w klasie Horse nadpisaliśmy
    }

    @Override
    public String getRidingDetails() {
        return "W siodle";
    }
}
