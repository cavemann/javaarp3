package exam.casting;

import org.apache.log4j.Logger;

public class Animal {
    private static final Logger LOGGER = Logger.getLogger(Animal.class);
    private String name;

    public Animal(String name) {
        this.name = name;
    }

    public void move() {
        LOGGER.info("Animal " + name + " is moving");
    }

}
