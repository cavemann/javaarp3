package exam.casting;

public interface Riding {
    default String getRidingDetails() {
        return "Na oklep";
    }

    default int numberOfRides() {
        return 5;
    }
}
