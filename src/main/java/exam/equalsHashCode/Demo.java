package exam.equalsHashCode;

import org.assertj.core.groups.Tuple;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class Demo {

    public static void main(String[] args) {

        Car car = new Car("Opel", 1992);
        Car car1 = new Car("Opel", 1992);
        Car car3 = new Car("VM", 1992);
        Car car2 = car;


        //Object identity - every object in Java has unique identity, even if they are defined same
        System.out.println(car1 == car2);
        //This is true because car2 has same reference as car, those two variables points to the same object
        System.out.println(car2 == car);

        /*Map<Car, String> carOwnerMap = new HashMap<>();
        carOwnerMap.put(car, "Oskar");
        carOwnerMap.put(car, "Oskar");
        carOwnerMap.put(car1, "Marek");


        System.out.println(car.hashCode());
        System.out.println(car1.hashCode());
        System.out.println(car3.hashCode());

        System.out.println(carOwnerMap.get(car));
        */

        //Jeśli zakomentujemy metodę hashCode to w hashMapie będziemy mieli duplikaty mimo, że HashMapa nie przyjmuje duplikatów.
        // Jeśli odkomentujemy metodę hashCode to obiekt car1 nadpisze duplikat jaki był car i w HashMapie będzie tylko 1 element
        Map<Car, String> carOwnerMap = new HashMap<>();
        System.out.println(car.hashCode());
        System.out.println(car1.hashCode());
        carOwnerMap.put(car, "Michal");
        carOwnerMap.put(car1, "Oskar");
        System.out.println(car.equals(car1));

        System.out.println(carOwnerMap.get(car));
        System.out.println(carOwnerMap.get(car1));
        System.out.println(carOwnerMap.size());

        Map<Map<Car, String>, String> carOwnerByCarAndOwner = new HashMap<>();
        Map<Car, String> ownerOfCar = new HashMap<>();
        ownerOfCar.put(car1, "Michal");
        carOwnerByCarAndOwner.put(ownerOfCar, "Michal");

        Map<String, String> carMap = new HashMap<>();
        carMap.put(car.getProductionYear()+ car.getBrand()+"Michal", "Michal");
        carMap.put(car1.getProductionYear()+ car1.getBrand()+"Oskar", "Oskar");


        //chcemy zmienić właściciela samochodu Opel z 1992
        carOwnerMap.put(car, "Wiesław");

        Optional<Car> optionalCar = carOwnerMap.keySet()
                .stream()
                .filter(each -> "Opel".equals(each.getBrand()) && 1992 == each.getProductionYear())
                .findFirst();
        if (optionalCar.isPresent()) {
            System.out.println("Właściciel Opla: " + carOwnerMap.get(optionalCar.get()));
        }

    }

}
