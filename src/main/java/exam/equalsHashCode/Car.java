package exam.equalsHashCode;


public class Car {

    private String brand;
    private int productionYear;

    public Car(String brand, int productionYear) {
        this.brand = brand;
        this.productionYear = productionYear;
    }

    public String getBrand() {
        return brand;
    }

    public int getProductionYear() {
        return productionYear;
    }

    @Override
    public boolean equals(Object object) {
        if (object == null) {
            return false;
        }

        if (this == object) {
            return true;
        }

        if (object instanceof Car) {

            Car o = (Car) object;

            if (brand.equals(o.brand) && productionYear == o.productionYear) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }
//
//    @Override
//    public int hashCode() {
//        int result = 7;
//        result = 31 * result + brand.hashCode();
//        result = 31 * result + productionYear;
//        return result;
//
//        //31 bo jest liczba pierwsza, jest nieparzysta i jak bedziemy chcieli mnozyc to hardware zrealizuje to jako przesuniecie bitowe i dodawanie - zwieksza to szybkosc
//        // oraz zapewnia lepsza dystybucje hashy
//    }

}
