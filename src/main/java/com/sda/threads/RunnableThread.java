package com.sda.threads;

import static com.sda.threads.ThreadColor.ANSI_RED;

public class RunnableThread implements Runnable {

    @Override
    public void run() {
        System.out.println(ANSI_RED + "Przykład wątku odpalanego interfejsem Runnable");
    }
}
