package com.sda.threads;

import static com.sda.threads.ThreadColor.*;

public class MainThread {
    public static void main(String[] args) {
        /**
         * Kolejność działąnia wątków, a co za tym idzie, kolejność
         * wyświetlania komunikatów na ekranie jest nieokreślona
         * Zależy od przydzielania zasobów poszczególnym wątkom przez procesor itp.
         */

        System.out.println(ANSI_PURPLE + " Działamy w głównym wątku programu");

        OtherThread otherThread = new OtherThread();
        otherThread.setName("=======  Osobny wątek na klasie THREAD ========");
        /**
         * Metoda run() zawiera kod który ma się wykonać w osobnym wątku
         * Metoda start() uruchamia osobny wątek i gdzieś później w tym wątku odpala się run()
         * Jeśli nie wywołamy wątku przez start() tylko zamiast tego użyjemy run() to kod
         * będzie wykonywał się bez podziału na wątki
         */
        otherThread.start();
        //otherThread.run();

        //otherThread.interrupt(); - przerywa działanie metody sleep() wywołanego wątku (w tym wypadku otherThread)

        /**
         * Wątek uruchamiany na klasie implementującej Runnable
         */
        Thread runnableSample = new Thread(new RunnableThread());
        runnableSample.start();

        /**
         * Jeśli mamy jednorazową akcję do wykonania w osobnym wątku
         * to może nie trzeba tworzyć osobnej klasy
         * tylko wywołać ten wątek na klasie anonimowej jak poniżej
         */
        new Thread() {
            public void run() {
                System.out.println(ANSI_GREEN + " Jednorazowa akcja z klasy anonimowej");
            }
        }.start();

        /**
         * Łączenie wywołania różnych wątków tak aby wykonywały się jeden po drugim
         * Metoda join()
         * Zrobimy to na nadpisanej definicji RunnableThread
         */
        runnableSample = new Thread(new RunnableThread() {
            @Override
            public void run() {
                System.out.println(ANSI_CYAN + "Nadpisana klasa RunnableThread w akcji");
                try {
                    System.out.println(ANSI_CYAN + "RunnableThread będzie czekał na wykonanie OtherThread");
                    otherThread.join(6000);
                    System.out.println(ANSI_CYAN + "Kontynuuję w RunnableThread");
                } catch (InterruptedException exception) {
                    System.out.println(ANSI_CYAN + "Przerwałem OtherThreadowi bo to za długo trwało");
                }

            }
        });
        runnableSample.start();

        //runnableSample.interrupt();
        otherThread.interrupt();

        System.out.println(ANSI_PURPLE + "Koniec działania głównego wątku apki wielowątkowej");
    }
}
