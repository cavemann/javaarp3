package com.sda.threads;

import static com.sda.threads.ThreadColor.ANSI_BLUE;

public class OtherThread extends Thread {

    @Override
    public void run() {
        System.out.println(ANSI_BLUE + " Działamy na klasie " + OtherThread.class.getSimpleName());
        System.out.println(ANSI_BLUE + currentThread().getName());

        try {
            sleep(4000);
            System.out.println(ANSI_BLUE + " Po spaniu wątek kończy pracę");
        } catch (InterruptedException exception) {
            System.out.println(ANSI_BLUE + " Wątek " + currentThread().getName() + " OBUDZONY bezczelnie");
        }
    }
}
