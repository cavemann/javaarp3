package com.sda.collections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortingDemo {
    public static void main(String[] args) {
        Person person1 = new Person("Jan", "Kowalski", 45);
        Person person2 = new Person("Marian", "Bania", 52);
        Person person3 = new Person("Agnieszka", "Wróblewska", 80);
        Person person4 = new Person("Zofia", "Mika", 33);
        Person person5 = new Person("Łukasz", "Zachary", 25);

        List<Person> people = new ArrayList<>();
        people.add(person1);
        people.add(person2);
        people.add(person3);
        people.add(person4);
        people.add(person5);

        people.forEach(person -> System.out.println(person.getPersonInfo()));
        System.out.println("===============================================");
        Collections.sort(people); //sortuje kolekcję i ZOSTAWIA POSORTOWANĄ
        System.out.println("Ponownie drukujemy listę:");
        people.forEach(person -> System.out.println(person.getPersonInfo()));
        System.out.println("===============================================");
        Collections.sort(people, new PersonComparedByAge()); //sortuje kolekcję i ZOSTAWIA POSORTOWANĄ
//        Collections.sort(people, new PersonComparedByAge().reversed()); //sortowanie w odwrotnej kolejności dzięki reversed()
        System.out.println("Ponownie drukujemy listę, tym razem po wieku:");
        people.forEach(person -> System.out.println(person.getPersonInfo()));
        System.out.println("===============================================");
        Collections.sort(people, new PersonComparedByName()); //sortuje kolekcję i ZOSTAWIA POSORTOWANĄ
//        Collections.sort(people, new PersonComparedByAge().reversed()); //sortowanie w odwrotnej kolejności dzięki reversed()
        System.out.println("Ponownie drukujemy listę, tym razem po wieku:");
        people.forEach(person -> System.out.println(person.getPersonInfo()));

        System.out.println("===============================================");
        System.out.println("===============================================");
        System.out.println("Sortowanie w stream za pomocą domyślnego komparatora:");
        people.stream()
                .sorted()
                .forEach(person -> System.out.println(person.getPersonInfo()));

        System.out.println("===============================================");
        System.out.println("===============================================");
        System.out.println("Sortowanie w stream za pomocą wskazanego komparatora:");
        people.stream()
                .sorted(new PersonComparedByName())
                .forEach(person -> System.out.println(person.getPersonInfo()));


        System.out.println("===============================================");
        System.out.println("===============================================");
        System.out.println("Sortowanie w stream za pomocą zdefiniowanego w locie komparatora:");
        people.stream()
                .sorted((x,y) -> {
                    if(x.getAge() != y.getAge()) {
                        return x.getAge() - y.getAge();
                    }
                    return x.getSurname().compareTo(y.getSurname());
                })
                .forEach(person -> System.out.println(person.getPersonInfo()));

    }
}
