package com.sda.collections;

import java.util.Comparator;

public class PersonComparedByAge implements Comparator<Person> {

    /**
     * Podobnie jak przy Comparable:
     * - zwrócona wartość mniejsza od zera - pierwszy obiekt "w pierwszej kolejności"
     * - zero - obiekty takie same (wg kryteriów porównywania)
     * - zwrócona wartość większa od zera - najpierw ustawiamy obiekt drugi
     */

    @Override
    public int compare(Person person1, Person person2) {
        return person1.getAge() - person2.getAge();
    }
}
