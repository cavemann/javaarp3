package com.sda.collections;

import java.time.LocalDate;

public class DateDemo {
    public static void main(String[] args) {
        LocalDate localDate = LocalDate.now();
        System.out.println(localDate.getDayOfWeek().getValue());
        System.out.println(localDate.getDayOfWeek().name());

    }
}
