package com.sda.collections;

/**
 * Do klasy przypisujemy interfejs Comparable
 * który wymusza implementację metody compareTo
 * i ma ona definiować "najbardziej dobmyślną", "naturalną"
 * metodę porównywania - klasyfikowania obiektów
 *
 * dla klasy Person np. Nazwisko i imię
 */

public class Person implements Comparable {
    private String name;
    private String surname;
    private int age;

    public Person(String name, String surname, int age) {
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getAge() {
        return age;
    }

    public String getPersonInfo() {
        return surname + " " + name + ", " +  age;
    }

    @Override
    public int compareTo(Object o) {
        if(o == null) {
            return -1;
        }
        if (!(o instanceof Person)) {
            return -1;
        }

        Person outer = (Person) o;

        /**
         * korzystamy z faktu że na klasie String jest obsłużony interfejs
         * Comparable i ma ona metodę compareTo do porównywania Stringów
         * Zwróci ona:
         * -1 jeśli pierwszy string jest "pierwszy w kolejności alfabetycznie"
         * 0 jeśli stringi są identyczne
         * 1 jeśli pierwszy string powienien być "później"
         */
        return (surname + name).compareTo(outer.getSurname() + outer.getName());
    }
}
