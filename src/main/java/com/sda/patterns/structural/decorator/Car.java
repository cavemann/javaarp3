package com.sda.patterns.structural.decorator;

public interface Car {

    void assemble();
}
