package com.sda.patterns.structural.decorator;

import org.apache.log4j.Logger;

public class DecoratorDemo {
    private static final Logger LOGGER = Logger.getLogger(DecoratorDemo.class);

    public static void main(String[] args) {

        Car basicCar = new BasicCar();
        basicCar.assemble();
        LOGGER.info("==========================================================");
        Car basicCar2 = new BasicCar();
        SportPackage sportPackage1 = new SportPackage(basicCar2, true, false);
        sportPackage1.assemble();
        LOGGER.info("==========================================================");
        Car basicCar3 = new BasicCar();
        SportPackage sportPackage2 = new SportPackage(basicCar3, true, true);
        WinterPackage winterPackage1 = new WinterPackage(sportPackage2, false, true);
        winterPackage1.assemble();
        LOGGER.info("==========================================================");
        /**
         * Pytanie z egzaminu: Jaki wzorzec projektowy jest wykorzystywany
         * przy wyołaniu takim jak:
         * new A(new B(new C()))
         * Dekorator
         */
        SportPackage sportPackage = new SportPackage(
                new WinterPackage(
                        new BasicCar(), false, false),
                        true, false);
        sportPackage.assemble();

        /**
         * Dodajcie klasę z pakietem rodzinnym
         *  - możliwość dołożenia isofixów
         *  - możliwość dołożenia haka
         *
         *  Zróbcie autko z pakietem zimowym i rodzinnym
         *
         */
    }
}
