package com.sda.patterns.structural.decorator;

import org.apache.log4j.Logger;

public class CarDecorator implements Car {
    private static final Logger LOGGER = Logger.getLogger(CarDecorator.class);

    protected Car car;

    public CarDecorator(Car car) {
        this.car = car;
    }

    @Override
    public void assemble() {
        LOGGER.info("Dokładamy do samochodu");
        this.car.assemble();
    }
}
