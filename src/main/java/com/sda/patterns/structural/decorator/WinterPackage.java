package com.sda.patterns.structural.decorator;

import org.apache.log4j.Logger;

public class WinterPackage extends CarDecorator {
    private static final Logger LOGGER = Logger.getLogger(WinterPackage.class);

    private boolean electricMirrors;
    private boolean heatedSeats;

    public WinterPackage(Car car, boolean electricMirrors, boolean heatedSeats) {
        super(car);
        this.electricMirrors = electricMirrors;
        this.heatedSeats = heatedSeats;
    }

    @Override
    public void assemble() {
        super.assemble();
        StringBuilder info = new StringBuilder("Pakiet zimowy")
                .append(electricMirrors ? " z podgrzewanymi lusterkami" : "")
                .append(heatedSeats ? " z podgrzewanymi siedzeniami" : "");
        LOGGER.info(info);
    }
}
