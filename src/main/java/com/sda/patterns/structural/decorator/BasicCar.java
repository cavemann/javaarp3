package com.sda.patterns.structural.decorator;

import org.apache.log4j.Logger;

public class BasicCar implements Car {
    private final static Logger LOGGER = Logger.getLogger(BasicCar.class);

    @Override
    public void assemble() {
        LOGGER.info("Składamy podstawową wersję samochodu");
    }
}
