package com.sda.patterns.structural.decorator;

import org.apache.log4j.Logger;

public class SportPackage extends CarDecorator {
    private static final Logger LOGGER = Logger.getLogger(SportPackage.class);
    private boolean sportWheel;
    private boolean sportSeats;

    public SportPackage(Car car, boolean sportWheel, boolean sportSeats) {
        super(car);
        this.sportWheel = sportWheel;
        this.sportSeats = sportSeats;
    }

    @Override
    public void assemble() {
        super.assemble();
        LOGGER.info("Dodajemy sportowe wyposażenie: " + (sportWheel ? " kierownica" : "") +
                (sportSeats ? " sportowe siedzenia" : ""));

    }
}
