package com.sda.patterns.structural.proxy;

import org.apache.log4j.Logger;

public class PrivateCompanyInternetNetwork implements CompanyInternetNetwork {
    private final static Logger LOGGER = Logger.getLogger(PrivateCompanyInternetNetwork.class);

    private String username;

    public PrivateCompanyInternetNetwork(String username) {
        this.username = username;
    }

    @Override
    public void getAccess(String username) {
        LOGGER.info("Nadany dostęp do wewnętrznych zasobów firmy dla " + username);
    }
}
