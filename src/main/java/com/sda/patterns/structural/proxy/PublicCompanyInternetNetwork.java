package com.sda.patterns.structural.proxy;

import org.apache.log4j.Logger;

public class PublicCompanyInternetNetwork implements CompanyInternetNetwork {
    private static final Logger LOGGER = Logger.getLogger(PublicCompanyInternetNetwork.class);

    private String username;

    public PublicCompanyInternetNetwork(String username) {
        this.username = username;
    }

    @Override
    public void getAccess(String username) {
        LOGGER.info("Tylko dostęp do własnej poczty i WWWW dla " + username);
    }
}
