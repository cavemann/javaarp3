package com.sda.patterns.structural.proxy;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ProxyCompanyInternetNetwork implements CompanyInternetNetwork {

    private static final Logger LOGGER = Logger.getLogger(ProxyCompanyInternetNetwork.class);

    CompanyInternetNetwork internetNetwork;

    @Override
    public void getAccess(String username) {
        if(isEmployee(username)) {
            internetNetwork = new PrivateCompanyInternetNetwork(username);
        } else {
            internetNetwork = new PublicCompanyInternetNetwork(username);
        }
        internetNetwork.getAccess(username);
    }

    private boolean isEmployee(String username) {
        return CompanyEmployees.isActiveEmployee(username);
    }

    List<String> jakasLista = new ArrayList<>();
    Set<String> someSet = new HashSet<>();

}
