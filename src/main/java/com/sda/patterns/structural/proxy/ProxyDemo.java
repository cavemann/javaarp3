package com.sda.patterns.structural.proxy;

import java.util.Arrays;
import java.util.List;

/**
 * Proxy jest po to żeby nałożyć dodatkową warstwę zabezpieczającą (to nasz przypadek)
 * lub warstwę cache (pamięci podręcznej)
 *
 * Tutaj, dzięki proxy mamy jeden punkt wejścia do sieci WWW, a w trakcie logowania
 * algorytm sam zdecyduje jaki dostęp użytkownik dostanie - czy pełny (private) czy okrojony (public)
 */

public class ProxyDemo {
    public static void main(String[] args) {
        CompanyEmployees companyEmployees = new CompanyEmployees();
        List<String> employees = Arrays.asList("Marian Kowalski", "Wiesława Gierek", "Robert Nowowolski", "Zofia Ryc");
        companyEmployees.setEmployees(employees);

        CompanyInternetNetwork internetNetwork = new ProxyCompanyInternetNetwork();
        internetNetwork.getAccess("Marian Kowalski");
        internetNetwork.getAccess("Krzysztof Wielenda");
        internetNetwork.getAccess("Aldona Pięta");
        internetNetwork.getAccess("Zofia Ryc");
    }
}
