package com.sda.patterns.structural.facade;

import java.sql.Connection;

public class FacadeDemo {
    public static void main(String[] args) {
        String tableName = "employee";

        //generujemy raport z bazy MySQL, do PDFa
        Connection connection = MySQLReportGenerator.getMySQLDbConnection();
        MySQLReportGenerator generator = new MySQLReportGenerator();
        generator.generateMySQL_PDFReport(tableName, connection);

        //generujemy raport z Oracla do HTMLa
        Connection connection2 = OracleReportGenerator.getOracleDBConnection();
        OracleReportGenerator reportGenerator = new OracleReportGenerator();
        reportGenerator.generateOracle_HTMLReport(tableName, connection2);

        /**
         * Powyżej mamy podejście typu:
         *  - klient chce raport
         *  - my każemy klientowi podłączyć się do bazy
         *  - następnie ma on utworzyć obiekt generatora raportów
         *  - i finalnie z tego generatora uruchomić tworzenie raportów
         *
         * Przez analogię:
         *  - klient chce BigMaca
         *  - każemy mu wejść za ladę, pokazujemy gdzie bułki
         *  - pokazujemy gdzie ma sobie dołożyć "kotlet"
         *  - podobnie z warzywami
         *  - jak już skończy to kasujemy go za "usługę"
         */

        /**
         * A poniżej robimy to samo ale z wykorzystaniem fasady
         */

        ReportFacade.generateReport(ReportFacade.DBType.MYSQL, ReportFacade.ReportType.HTML, tableName);

        ReportFacade.generateReport(ReportFacade.DBType.ORACLE, ReportFacade.ReportType.PDF, tableName);
    }
}
