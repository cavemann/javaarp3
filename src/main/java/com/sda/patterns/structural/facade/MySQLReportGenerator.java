package com.sda.patterns.structural.facade;

import org.apache.log4j.Logger;

import java.sql.Connection;

public class MySQLReportGenerator {
    private static final Logger LOGGER = Logger.getLogger(MySQLReportGenerator.class);

    public static Connection getMySQLDbConnection() {
        LOGGER.info("Połączenie z basą MySQL");
        return null;
    }

    public MySQLReportGenerator() {
        LOGGER.info("Konstruktor MySQLReportGenerator");
    }

    public void generateMySQL_PDFReport(String table, Connection connection){
        LOGGER.info("Tworzymy raport z bazy MySQL, do PDFa");
    }

    public void generateMySQL_HTMLReport(String table, Connection connection) {
        LOGGER.info("Tworzymy raport z bazy MySQL, do HTMLa");
    }
}
