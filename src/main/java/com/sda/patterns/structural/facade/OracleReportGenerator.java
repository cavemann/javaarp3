package com.sda.patterns.structural.facade;

import org.apache.log4j.Logger;

import java.sql.Connection;

public class OracleReportGenerator {
    private static final Logger LOGGER = Logger.getLogger(OracleReportGenerator.class);

    public static Connection getOracleDBConnection() {
        LOGGER.info("Połączenie z basą Oracle");
        return null;
    }

    public OracleReportGenerator() {
        LOGGER.info("Konstruktor OracleReportGenerator");
    }

    public void generateOracle_PDFReport(String table, Connection connection){
        LOGGER.info("Tworzymy raport z bazy Oracle, do PDFa");
    }

    public void generateOracle_HTMLReport(String table, Connection connection) {
        LOGGER.info("Tworzymy raport z bazy Oracle, do HTMLa");
    }
}
