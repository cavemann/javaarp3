package com.sda.patterns.structural.facade;

import java.sql.Connection;

public class ReportFacade {

    public static void generateReport(DBType dbType, ReportType reportType, String tableName) {
        Connection connection = null;

        switch (dbType) {
            case ORACLE:
                connection = OracleReportGenerator.getOracleDBConnection();
                OracleReportGenerator generator = new OracleReportGenerator();
                switch (reportType) {
                    case HTML:
                        generator.generateOracle_HTMLReport(tableName, connection);
                        break;
                    case PDF:
                        generator.generateOracle_PDFReport(tableName, connection);
                        break;
                }
                break;
            case MYSQL:
                connection = MySQLReportGenerator.getMySQLDbConnection();
                MySQLReportGenerator generator1 = new MySQLReportGenerator();
                switch (reportType) {
                    case HTML:
                        generator1.generateMySQL_HTMLReport(tableName, connection);
                        break;
                    case PDF:
                        generator1.generateMySQL_PDFReport(tableName, connection);
                        break;
                }
                break;
        }
    }

    enum DBType {
        ORACLE,
        MYSQL
    }

    enum ReportType {
        PDF,
        HTML
    }
}
