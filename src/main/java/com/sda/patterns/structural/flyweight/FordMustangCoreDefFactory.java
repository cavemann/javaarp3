package com.sda.patterns.structural.flyweight;

import java.util.HashSet;
import java.util.Set;

public class FordMustangCoreDefFactory {

    //przechowalnia na nasze zestawy "mniejszych obiektów" - tych często powtarzających się
    private static Set<FordMustangCoreDef> fordMustangCoreDefSet = new HashSet<>();

    public static FordMustangCoreDef getCoreDef(String color, String engine) {
        FordMustangCoreDef fordMustangCoreDef;
        System.out.println(FordMustangCoreDefFactory.class + " - w metodzie fabrycznej");

        if(fordMustangCoreDefSet.size() > 0) {
            //jeśli w przechowalni coś jest
            System.out.println("CoreDefSet już coś zawiera");
            fordMustangCoreDef = fordMustangCoreDefSet.stream()
                    //szukamy kombinacji kolor - silnik zgodnej z aktualnym zamówieniem
                    .filter(each -> color.equals(each.getColor()) && engine.equals(each.getEngine()))
                    //jeśli znajdziemy to ją wyświetlamy
                    .peek(found -> System.out.println("Znalesiony Core : " + found.getColor() + ", " + found.getEngine()))
                    //a następnie zwracamy
                    .findAny()
                    //orElseGet - jeśli nie znajdziemy takiego zestawu, to tworzymy go od zera
                    .orElseGet(() -> new FordMustangCoreDef(color, engine));
        } else {
            //jeśli w przechowalni nic nie ma to zawsze robimy nowy obiekt
            System.out.println("Dodajemy pierwszy wpis CoreDef");
            fordMustangCoreDef = new FordMustangCoreDef(color, engine);
        }
        //zawsze dodajemy obiekt, ale trzymamy je w Set więc jeśli dodamy powtarzający się to i tak nic się nie stanie
        fordMustangCoreDefSet.add(fordMustangCoreDef);
        return fordMustangCoreDef;
    }

    public static Set<FordMustangCoreDef> getFordMustangCoreDefSet() {
        return fordMustangCoreDefSet;
    }
}
