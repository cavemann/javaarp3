package com.sda.patterns.structural.flyweight;

import java.util.Arrays;
import java.util.List;

public class FlyweightDemo {
    public static void main(String[] args) {

        FordMustang mustang1 = new FordMustang("Midnight Sky", "4,5", "Denon");
        FordMustang mustang2 = new FordMustang("Ruby Red", "4,0", "Sony");
        FordMustang mustang3 = new FordMustang("Midnight Sky", "3,2", "Ford");
        FordMustang mustang4 = new FordMustang("Black Panther", "4,4", "Denon");
        FordMustang mustang5 = new FordMustang("Midnight Sky", "4,5", "Ford");

        List<FordMustang> mustangList = Arrays.asList(mustang1, mustang2, mustang3, mustang4, mustang5);
        System.out.println("==================================");
        System.out.println("Drukujemy listę mustangów");
        mustangList.forEach(each -> System.out.println(each.carInfo()));

        System.out.println("==================================");
        System.out.println("Elementy z fabryki : ");
        FordMustangCoreDefFactory.getFordMustangCoreDefSet()
                .stream()
                .forEach(each -> System.out.println(each.getColor() + " " + each.getEngine()));


    }
}
