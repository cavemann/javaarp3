package com.sda.patterns.structural.flyweight;

public class FordMustangCoreDef {
    private String color;
    private String engine;

    public FordMustangCoreDef(String color, String engine) {
        System.out.println(this.getClass() + " - konstruktor");
        this.color = color;
        this.engine = engine;
    }

    public String getColor() {
        return color;
    }

    public String getEngine() {
        return engine;
    }
}
