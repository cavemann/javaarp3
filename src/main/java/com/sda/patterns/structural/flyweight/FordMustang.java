package com.sda.patterns.structural.flyweight;

/**
 * Wzorzec flyweight powstał w celu rozwiązania problemów z pamięcią
 * gdzie w obiektach można wyróżnić elementy powtarzające się i wyodrębnić
 * je do osobnego "podobiektu"
 *
 * https://java-arp.pl.sdacademy.pro/e-podrecznik/inzynieria_oprogramowania_i_dobre_praktyki/wzorce_strukturalne/flyweight/
 * 1 akapit i pierwsze dwa zdania drugiego - mniej więcej tego dotyczyło pytanie na kursie
 */

public class FordMustang {

    private FordMustangCoreDef fordMustangCoreDef;

    private String radio;

    public FordMustang(String color, String engine, String radio) {
        System.out.println(this.getClass() + " - konstruktor");
        this.fordMustangCoreDef = FordMustangCoreDefFactory.getCoreDef(color, engine);
        this.radio = radio;
    }

    public String carInfo() {
        return "Ford Mustang: kolor - " + fordMustangCoreDef.getColor()
                + ", silnik - " + fordMustangCoreDef.getEngine()
                + ", radio - " + radio;
    }
}
