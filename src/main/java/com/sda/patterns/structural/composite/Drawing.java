package com.sda.patterns.structural.composite;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class Drawing implements Shape {
    private final static Logger LOGGER = Logger.getLogger(Drawing.class);

    private List<Shape> shapes;
    private String name;

    public Drawing(String name) {
        this.name = name;
        this.shapes = new ArrayList<>();
    }

    public boolean addShape(Shape shape) {
        if(!shapes.contains(shape)) {
            return shapes.add(shape);
        }
        return false;
    }

    @Override
    public void draw() {
        LOGGER.info("Drawing " + name + " and it's parts");
        shapes.forEach(Shape::draw);
    }
}
