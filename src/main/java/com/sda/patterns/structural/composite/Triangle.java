package com.sda.patterns.structural.composite;

import org.apache.log4j.Logger;

public class Triangle implements Shape {
    private static final Logger LOGGER = Logger.getLogger(Triangle.class);

    @Override
    public void draw() {
        LOGGER.info("Printing triangle");
    }
}
