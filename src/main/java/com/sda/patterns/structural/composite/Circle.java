package com.sda.patterns.structural.composite;

import org.apache.log4j.Logger;

public class Circle implements Shape {
    private static final Logger LOGGER = Logger.getLogger(Circle.class);

    @Override
    public void draw() {
        LOGGER.info("Drawing circle");
    }
}
