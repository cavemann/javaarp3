package com.sda.patterns.structural.composite;

public interface Shape {
    void draw();
}
