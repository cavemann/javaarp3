package com.sda.patterns.structural.composite;

import org.apache.log4j.Logger;

/**
 * Composite czyli kompozycja
 * czyli jak mamy obiekty proste (koło, trójkąt) i bardziej złożone (drawing)
 * to mimo wszystko możemy na nich wykonywać te same akcje po nałożeniu odpowiedniego interfejsu
 */

public class CompositeDemo {
    private static final Logger LOGGER = Logger.getLogger(CompositeDemo.class);

    public static void main(String[] args) {

        /**
         * Wzorzec składający struktury "część - całość"
         */

        Shape triangle1 = new Triangle();
        Shape triangle2 = new Triangle();

        Shape circle1 = new Circle();
        Shape circle2 = new Circle();

        Drawing drawing1 = new Drawing("Rysunek 1");
        Shape drawing2 = new Drawing("Rysunek 2");


        drawing1.addShape(triangle1);
        drawing1.addShape(circle1);


        ((Drawing) drawing2).addShape(circle2);
        ((Drawing) drawing2).addShape(triangle2);
        ((Drawing) drawing2).addShape(drawing1);

        drawing1.draw();
        LOGGER.info("===============================================");
        LOGGER.info("===============================================");
        drawing2.draw();
    }
}
