package com.sda.patterns.behavioral.command;

public class MyFile {

    private String filename;
    private String content;

    public MyFile(String filename) {
        this.filename = filename;
    }

    public String createContent(String content) {
        this.content = content;
        return "Creating file content: '" + content + "'.";
    }

    public String updateContent(String newContent) {
        this.content += newContent;
        return "Adding file content: '" + newContent + "'";
    }

    public String getFilename() {
        return filename;
    }

    public String getContent() {
        return content;
    }
}
