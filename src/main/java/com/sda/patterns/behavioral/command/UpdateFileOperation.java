package com.sda.patterns.behavioral.command;

public class UpdateFileOperation implements FileOperation {
    private MyFile myFile;

    public UpdateFileOperation(MyFile myFile) {
        this.myFile = myFile;
    }

    @Override
    public String performOperation(String content) {
        System.out.println(getClass().getSimpleName() + " przetwarza " + content);
        return myFile.updateContent(content);
    }
}
