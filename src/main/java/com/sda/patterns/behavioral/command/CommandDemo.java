package com.sda.patterns.behavioral.command;

public class CommandDemo {

    public static void main(String[] args) {
        MyFile myFile = new MyFile("DemoFile.txt");
        FileOperationTaskManager taskManager = new FileOperationTaskManager();

        taskManager.executeOperation(new CreateFileOperation(myFile), "Pierwsza linia");
        System.out.println(myFile.getContent());
        System.out.println("==========================");
        taskManager.executeOperation(new UpdateFileOperation(myFile), "Druga linia");
        System.out.println(myFile.getContent());
        System.out.println("==========================");
        taskManager.executeOperation(new UpdateFileOperation(myFile), "Trzecia linia");
        System.out.println(myFile.getContent());
        System.out.println("==========================");
        taskManager.executeOperation(new CreateFileOperation(myFile), "Nowa pierwsza linia");
        System.out.println(myFile.getContent());
        System.out.println("==========================");
        System.out.println("==== HISTORIA OPERACJI ====");
        taskManager.getFileOperationHistory()
                .forEach(System.out::println);

    }

}