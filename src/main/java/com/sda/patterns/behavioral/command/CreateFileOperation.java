package com.sda.patterns.behavioral.command;

public class CreateFileOperation implements FileOperation {
    private MyFile myFile;

    public CreateFileOperation(MyFile myFile) {
        this.myFile = myFile;
    }

    @Override
    public String performOperation(String content) {
        System.out.println(getClass().getSimpleName() + " przetwarza " + content);
        return myFile.createContent(content);
    }
}
