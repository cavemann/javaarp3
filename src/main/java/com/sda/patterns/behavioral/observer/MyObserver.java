package com.sda.patterns.behavioral.observer;

public class MyObserver implements Observer {

    private String name;
    private Subject subject;

    public MyObserver(String name) {
        this.name = name;
    }

    @Override
    public void update() {
        String msg = (String) subject.getUpdate();
        if(msg == null) {
            System.out.println(name + ":: no new messages.");
        } else {
            System.out.println(name + ":: New stuff to read: " + msg);
        }
    }

    @Override
    public void setSubject(Subject subject) {
        this.subject = subject;
    }
}
