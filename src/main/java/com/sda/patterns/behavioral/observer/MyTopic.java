package com.sda.patterns.behavioral.observer;

import java.util.ArrayList;
import java.util.List;

public class MyTopic implements Subject {

    private List<Observer> observerList;
    private String message;
    private boolean changed;

    public MyTopic() {
        observerList = new ArrayList<>();
    }

    @Override
    public void register(Observer observer) {
        if(!observerList.contains(observer)) {
            System.out.println("Adding observer " + observer + " to follow topic");
            observerList.add(observer);
        }
    }

    @Override
    public void unregister(Observer observer) {
        observerList.remove(observer);
    }

    @Override
    public void notifyObservers() {
        if(!changed) {
            return;
        }
        List<Observer> observers = List.copyOf(this.observerList);
        observers.forEach(Observer::update);
        changed = false;
    }

    @Override
    public Object getUpdate() {
        return this.message;
    }

    public void postMessage(String message) {
        System.out.println("New message posted: '" + message + "'");
        this.message = message;
        this.changed = true;
        notifyObservers();
    }
}
