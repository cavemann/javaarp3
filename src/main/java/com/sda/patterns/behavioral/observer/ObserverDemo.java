package com.sda.patterns.behavioral.observer;

public class ObserverDemo {

    public static void main(String[] args) {

        Observer observer1 = new MyObserver("Arek");
        Observer observer2 = new MyObserver("Kasia");
        Observer observer3 = new MyObserver("Jurek");

        Subject subject = new MyTopic();
        //rejestracja obserwujących do tematu
        observer1.setSubject(subject);
        observer2.setSubject(subject);
        subject.register(observer1);
        subject.register(observer2);

        System.out.println("==============================================");
        ((MyTopic) subject).postMessage("Nie uwierzycie co odkryli amerykańscy naukowcy");
        System.out.println("==============================================");
        observer3.setSubject(subject);
        subject.register(observer3);
        ((MyTopic) subject).postMessage("Dramat Ronaldo - dostał 3M podwyżki a chciał 6");
        System.out.println("==============================================");
        subject.unregister(observer2);
        ((MyTopic) subject).postMessage("Boguś Linda nie może pić");
        System.out.println("==============================================");

    }
}
