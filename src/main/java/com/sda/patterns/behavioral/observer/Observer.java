package com.sda.patterns.behavioral.observer;

/**
 * Pytanie testowe - "wzorzec definiujący relację jeden do wielu" - observer / obserwator
 */

public interface Observer {

    void update();

    void setSubject(Subject subject);
}
