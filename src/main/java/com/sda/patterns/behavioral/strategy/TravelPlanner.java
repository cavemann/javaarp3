package com.sda.patterns.behavioral.strategy;

public class TravelPlanner {

    private String startPoint;
    private String destinationPoint;
    private String finalPlan;

    public TravelPlanner(String startPoint, String destinationPoint) {
        this.startPoint = startPoint;
        this.destinationPoint = destinationPoint;
    }

    public void setFinalPlan(TravelStrategy travelStrategy) {
        finalPlan = travelStrategy.setTravelPlan(startPoint, destinationPoint);
    }

    public void getTravelInfo() {
        System.out.println("Travelling from '" + startPoint + "' to '" + destinationPoint);
        System.out.println("========== DETAILS ==========");
        System.out.println(finalPlan);
        System.out.println("===============================");
    }
}
