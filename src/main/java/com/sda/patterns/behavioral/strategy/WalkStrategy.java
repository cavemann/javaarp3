package com.sda.patterns.behavioral.strategy;

public class WalkStrategy implements TravelStrategy {
    private boolean includeTouristicPaths;

    public WalkStrategy(boolean includeTouristicPaths) {
        this.includeTouristicPaths = includeTouristicPaths;
    }

    @Override
    public String setTravelPlan(String from, String to) {
        return new StringBuilder("Travel by foot from '")
                .append(from)
                .append("' to '")
                .append(to)
                .append("'")
                .append(includeTouristicPaths ? " z uwzględnieniem szlaków turystycznych" : "")
                .toString();

    }
}
