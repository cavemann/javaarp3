package com.sda.patterns.behavioral.strategy;

public class StrategyDemo {
    public static void main(String[] args) {

        /**
         * Algorytm postępowania - strategy albo templateMethod
         */

        TravelPlanner planner = new TravelPlanner("Wrocław", "Góry Izerskie");
        planner.setFinalPlan(new WalkStrategy(true));
        planner.getTravelInfo();
        System.out.println("\n============================================================\n");

        planner.setFinalPlan(new CarStrategy(false, true));
        planner.getTravelInfo();
        System.out.println("\n============================================================\n");

        TravelPlanner holidayTrip = new TravelPlanner("Tu gdzie jestem", "Bieszczady");
        holidayTrip.setFinalPlan(new CarStrategy(false, true));
        holidayTrip.getTravelInfo();
        System.out.println("\n============================================================\n");
        holidayTrip.setFinalPlan(new WalkStrategy(true));
        holidayTrip.getTravelInfo();
    }
}
