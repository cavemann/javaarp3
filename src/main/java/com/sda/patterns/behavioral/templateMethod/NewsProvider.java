package com.sda.patterns.behavioral.templateMethod;

public abstract class NewsProvider {
    private String message;

    public abstract boolean authorize();

    /**
     * Metoda final, nie będzie możliwości nadpisania jej
     * czyli kod takiej metody się nie zmieni
     */
    public final boolean sendMessage() {
        System.out.println("Message: '" + message + "' sent!");
        return true;
    }

    public abstract boolean endConnection();

    /**
     * Metoda final zawierająca algorytm postępowania - sposób realizacji algorytmu jest niezmienny
     * Część wywołanych metod jest abstrakcyjna, do zdefiniowania dla
     * konkretnych zastosowań
     * a część jest final, czyli takie jak są, takie już zostaną
     */
    public final void provideNews() {
        if(authorize()) {
            System.out.println("Authorized, sending message");
            sendMessage();
            endConnection();
        } else {
            System.out.println("Not authorized, not sending message");
        }
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
