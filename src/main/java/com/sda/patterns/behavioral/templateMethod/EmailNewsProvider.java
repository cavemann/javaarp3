package com.sda.patterns.behavioral.templateMethod;

public class EmailNewsProvider extends NewsProvider {
    private String email;

    public EmailNewsProvider(String email) {
        this.email = email;
    }

    @Override
    public boolean authorize() {
        if(email.contains("@")) {
            System.out.println("Mail prawidłowy");
            return true;
        }
        System.out.println("Mail nieprawidłowy");
        return false;
    }

    @Override
    public boolean endConnection() {
        System.out.println("Finishing mail sending");
        return true;
    }
}
