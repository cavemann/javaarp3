package com.sda.patterns.behavioral.templateMethod;

public class PushNewsProvider extends NewsProvider {
    String phoneNumber;

    public PushNewsProvider(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    @Override
    public boolean authorize() {
        if(phoneNumber.startsWith("+48")) {
            System.out.println("Telefon prawidłowy");
            return true;
        }
        return false;
    }

    @Override
    public boolean endConnection() {
        System.out.println("Zwolnienie kanału do PUSHY");
        return true;
    }
}
