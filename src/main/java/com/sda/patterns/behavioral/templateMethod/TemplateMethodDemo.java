package com.sda.patterns.behavioral.templateMethod;

public class TemplateMethodDemo {
    public static void main(String[] args) {

        /**
         * Było takie pytanie testowe:
         * Wzorzec gdzie algorytm jest określony, część jego metod / składowych niezmienna
         * a część dostępna do oprogramowania to ... TEMPLATE METHOD
         */

        NewsProvider firstProvider = new EmailNewsProvider("aaa@bbb.cc");
        firstProvider.setMessage("Jakis news");
        firstProvider.provideNews();

        System.out.println("====================================");
        NewsProvider secondProvider = new PushNewsProvider("+48111222333");
        secondProvider.setMessage("xys test 123");
        secondProvider.provideNews();
    }
}
