package com.sda.patterns.creational.prototype;

import java.util.ArrayList;
import java.util.List;

/**
 * Prototype jest wzorcem zakładającym instnienie
 * jakiegoś "wzorcowego" obiektu
 * Przy tworzeniu nowego obiektu kopiujemy ten
 * wzorcowy i w swoim robimy niezbędne modyfikacje -
 * przestawiamy te parametry które są nam niezbędne a
 * we wzorcu były ustawione inaczej
 */
public class ShoppingList implements Cloneable {
    private List<String> shoppings;

    public ShoppingList() {
        this.shoppings = new ArrayList<>();
        loadInitialData();
    }

    public ShoppingList(List<String> shoppings) {
        this.shoppings = shoppings;
    }

    private void loadInitialData() {
        shoppings.add("Chleb");
        shoppings.add("Masło");
        shoppings.add("Pomidory");
    }

    public List<String> getShoppings() {
        return shoppings;
    }

    public void setShoppings(List<String> shoppings) {
        this.shoppings = shoppings;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        List<String> currentShopings =  List.copyOf(shoppings);
        return new ShoppingList(currentShopings);
    }
}
