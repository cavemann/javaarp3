package com.sda.patterns.creational.prototype;

import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ShoppingsDemo {
    private final static Logger LOGGER = Logger.getLogger(ShoppingsDemo.class);

    public static void main(String[] args) {
        ShoppingList defaultShopping = new ShoppingList();

        try {
            ShoppingList shoppingForMonday = (ShoppingList) defaultShopping.clone();
            /**
             * new ArrayList jest potrzebne bo
             * tworząc "kopię" obiektu shoppingsForMonday
             * do utworzenia listy zakupów użyliśmy metody List.copyOf
             * która tworzy listę ale NIEMUTOWALNĄ czyli taką z której nic nie można
             * usunąć ani nic do niej dodać / zmodyfikować
             * new ArrayList tworzy nową, kolejną listę, z elementów tej niemutowalnej
             * ale już nie przenosi tej blokady zmian / usuwania / dodawania
             */
            List<String> mondayList = new ArrayList<>(shoppingForMonday.getShoppings());
            mondayList.remove("Masło");
            mondayList.add("Ser żółty");
            shoppingForMonday.setShoppings(mondayList);
            LOGGER.info("Zakupy na poniedziałek: ");
            shoppingForMonday.getShoppings().forEach(each -> LOGGER.info(each));
            LOGGER.info("==================================");
            LOGGER.info("==================================");
            ShoppingList shoppingForFriday = (ShoppingList) defaultShopping.clone();
            List<String> fridayList = new ArrayList<>(shoppingForFriday.getShoppings());
            fridayList.remove("Chleb");
            fridayList.add("Piwo");
            fridayList.add("Karkówka");
            shoppingForFriday.setShoppings(fridayList);
            LOGGER.info("Zakupy na piątel: ");
            shoppingForFriday.getShoppings().forEach(each -> LOGGER.info(each));

        } catch (Exception e) {
            LOGGER.error("Exception occured");
        }
    }
}
