package com.sda.patterns.creational.singleton;

import org.apache.log4j.Logger;

public class SingletonDemo {
    private static final Logger LOGGER = Logger.getLogger(SingletonDemo.class);

    public static void main(String[] args) {

        EagerSingleton eagerSingleton1 = EagerSingleton.getInstance();
        eagerSingleton1.setSomeClassField("Jakas wartosc");
        LOGGER.info("Wypisujemy ustawiona wartosc: " + eagerSingleton1.getSomeClassField());
        EagerSingleton.getInstance().setSomeClassField("Inna wartosc");
        LOGGER.info("Wypisujemy ustawiona wartosc: " + eagerSingleton1.getSomeClassField());

        EagerSingleton eagerSingleton2 = EagerSingleton.getInstance();
        eagerSingleton2.setSomeClassField("Wartosc ustawiona dla singletona2");

        LOGGER.info("Wypisujemy ustawiona wartosc na ES1: " + eagerSingleton1.getSomeClassField());
        LOGGER.info("Wypisujemy ustawiona wartosc na ES2: " + eagerSingleton2.getSomeClassField());

        LOGGER.info("=======================================================================");

        LazySingleton lazySingleton1 = LazySingleton.getInstance();
    }
}
