package com.sda.patterns.creational.singleton;

import org.apache.log4j.Logger;

/**
 * Singleton - wzorzec pozwalający na zapewnienie
 * utworzenia max jednego obiektu danej klasy
 *
 * Eager Singleton - obiekt jest tworzony od razu przy uruchamianiu / klasy (aplikacji)
 */

public final class EagerSingleton {
    private static final Logger LOGGER = Logger.getLogger(EagerSingleton.class);

    //prywatne pole EagerSingleton, od razu inicjowane
    private static final EagerSingleton INSTANCE = new EagerSingleton();

    private String someClassField;

    //prywatny konstruktor - nie mamy dostępu z zewnątrz (*)
    // * tak naprawdę jednak mamy np. REFLEKSJĄ
    private EagerSingleton() {
        LOGGER.info("Tworzymy instancję EagerSingleton");
    }

    //publiczna metoda zwracająca obiekt - w założeniu zawsze ten sam
    public static EagerSingleton getInstance() {
        LOGGER.info("Pobieramy obiekt klasy EagerSingleton");
        return INSTANCE;
    }

    public String getSomeClassField() {
        return someClassField;
    }

    public void setSomeClassField(String someClassField) {
        this.someClassField = someClassField;
    }
}
