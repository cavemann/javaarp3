package com.sda.patterns.creational.singleton;

import org.apache.log4j.Logger;

public class LazySingleton {
    private final static Logger LOGGER = Logger.getLogger(LazySingleton.class);

    //dla Lazy pole nie finalne
    private static LazySingleton INSTANCE;

    //konstruktor jak dla Eager
    private LazySingleton() {
        LOGGER.info("Tworzymy LazySingleton");

    }

    //pobieranie obiektu jeszcze sprawdza czy jest on zainicjowane
    //przy pierwszym pobraniu nie będzie
    //wtedy go tworzymy, potem już tylko pobieramy
    public static LazySingleton getInstance() {
        LOGGER.info("Zwracamy obiekt klasy LazySingleton");
        if (INSTANCE == null) {
            LOGGER.info("Jeśli jeszcze nie istnieje to go tworzymy");
            INSTANCE = new LazySingleton();
        }
        return INSTANCE;
    }
}
