package com.sda.patterns.creational.builder;

import org.apache.log4j.Logger;

import java.util.List;

public class HouseBuilderDemo {
    private static final Logger LOGGER = Logger.getLogger(HouseBuilderDemo.class);

    public static void main(String[] args) {
        House smallHouse = new House.HouseBuilder("bloczki", "Solbet", "blacha").build();

        House villageHouse = new House.HouseBuilder("ława", "Max", "Dachówka")
                .withGarden("działka na warzywa").build();

        House residence = new House.HouseBuilder("bloczki", "Max", "Dachówka")
                .withGarden("ogród z basenem")
                .withGarage("Wolnostojący dwustanowiskowy")
                .build();

        House twin = new House.HouseBuilder("ława", "Ytong", "Papa bitumiczna")
                .withGarage("W bryle budynku")
                .build();

        House penthouse = new House.HouseBuilder("bloczki", "Ytong", "Dachówka")
                .withGarage("W bryle")
                .withGarden("Mały ogród z kwiatami")
                .withSauna("Sauna ogrodowa")
                .build();

        List<House> houses = List.of(smallHouse, villageHouse, residence, twin, penthouse);
        houses.forEach(each -> LOGGER.info(each.getHouseInfo()));
    }
}
