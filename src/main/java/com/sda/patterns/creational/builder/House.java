package com.sda.patterns.creational.builder;

public class House {
    private String basement;
    private String walls;
    private String roof;

    private String garage;
    private String garden;
    private String sauna;

    /**
     * Konstruktor House na podstawie danych z HouseBuilder
     */
    public House(HouseBuilder builder) {
        this.basement = builder.basement;
        this.walls = builder.walls;
        this.roof = builder.roof;
        this.garage = builder.garage;
        this.garden = builder.garden;
        this.sauna = builder.sauna;
    }

    public String getHouseInfo() {
        return "House{" +
                "basement=" + basement +
                ", walls=" + walls +
                ", roof=" + roof +
                ", garage=" + garage +
                ", garden=" + garden +
                ", sauna=" + sauna +
                '}';
    }

    @Override
    public String toString() {
        return "House{" +
                "basement='" + basement + '\'' +
                ", walls='" + walls + '\'' +
                ", roof='" + roof + '\'' +
                ", garage='" + garage + '\'' +
                ", garden='" + garden + '\'' +
                ", sauna='" + sauna + '\'' +
                '}';
    }

    public static class HouseBuilder {
        private String basement;
        private String walls;
        private String roof;

        private String garage;
        private String garden;
        private String sauna;

        public HouseBuilder(String basement, String walls, String roof) {
            this.basement = basement;
            this.walls = walls;
            this.roof = roof;
            this.garden = "";
            this.garage = "";
            this.sauna = "";
        }

        public HouseBuilder withGarage(String garage) {
            this.garage = garage;
            return this;
        }

        public HouseBuilder withGarden(String garden) {
            this.garden = garden;
            return this;
        }

        public HouseBuilder withSauna(String sauna) {
            this.sauna = sauna;
            return this;
        }

        public House build() {
            return new House(this);
        }
    }
}
