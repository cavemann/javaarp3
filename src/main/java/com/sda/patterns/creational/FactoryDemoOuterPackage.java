package com.sda.patterns.creational;

import com.sda.patterns.creational.factory.MacBook;
import com.sda.patterns.creational.factory.MacBookFactory;
import com.sda.patterns.creational.factory.MacBookPro;

public class FactoryDemoOuterPackage {
    public static void main(String[] args) {
        MacBook macPro = MacBookFactory.getMac("pro", "256", "256", 13, "Intel");
        /**
         * W pakiecie innym niż com.sda.patterns.factory
         * czyli np w pakiecie com.sda.patterns, ale też w dowolnym innym
         * nie możemy skorzystać z konstruktorów MacBookPro i MacBookAir
         * bo są one package-private czyli są widoczne tylko w zakresie pakietu
         * com.sda.patterns.factory
         */
        //       MacBook macPro2 = new MacBookPro("pro", "256", "256", 13, "Intel");


    }
}
