package com.sda.patterns.creational.abstractFactory;

public class FordFocusFactory implements FordFactory {
    private String name;
    private String engine;

    FordFocusFactory(String name, String engine) {
        this.name = name;
        this.engine = engine;
    }

    @Override
    public Ford createFord() {
        return new FordFocus(this.name, this.engine);
    }
}
