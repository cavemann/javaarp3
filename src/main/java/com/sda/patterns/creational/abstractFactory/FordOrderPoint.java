package com.sda.patterns.creational.abstractFactory;

public class FordOrderPoint {

    public static Ford orderNewFord(FordFactory factory) {
        return factory.createFord();
    }
}
