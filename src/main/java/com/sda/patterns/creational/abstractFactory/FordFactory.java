package com.sda.patterns.creational.abstractFactory;

public interface FordFactory {
    Ford createFord();
}
