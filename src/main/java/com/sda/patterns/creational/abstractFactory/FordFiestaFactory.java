package com.sda.patterns.creational.abstractFactory;

public class FordFiestaFactory implements FordFactory {
    private String name;
    private String engine;

    FordFiestaFactory(String name, String engine) {
        this.name = name;
        this.engine = engine;
    }

    @Override
    public Ford createFord() {
        return new FordFiesta(this.name, this.engine);
    }
}
