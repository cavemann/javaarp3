package com.sda.patterns.creational.abstractFactory;

import org.apache.log4j.Logger;

/**
 * Fabryka abstrakcyjna jest wykorzystywana gdy w rodzinie obiektów stale pojawiają się nowe typy,
 * jak w samochodach: Ford miał Escorta, CMaxa, BMaxa, stale ma Focusa i Fiestę, ale też pojawiają
 * się nowe jak EcoSport(czy jakoś tak)
 */
public class FordFactoryDemo {

    private final static Logger LOGGER = Logger.getLogger(FordFactoryDemo.class);

    public static void main(String[] args) {
        Ford fiesta1 = FordOrderPoint.orderNewFord(new FordFiestaFactory("Fiesta Trend", "1.2"));
        Ford fiesta2 = FordOrderPoint.orderNewFord(new FordFiestaFactory("Fiesta ST Line", "1.8"));
        Ford focus1 = FordOrderPoint.orderNewFord(new FordFocusFactory("Focus Titanium", "1.6"));

        LOGGER.info("Fiesta1: " + fiesta1);
        LOGGER.info("Fiesta2: " + fiesta2);
        LOGGER.info("Focus2: " + focus1);
    }
}
