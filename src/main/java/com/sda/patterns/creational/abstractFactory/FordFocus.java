package com.sda.patterns.creational.abstractFactory;

public class FordFocus extends Ford {
    private String name;
    private String engine;

    FordFocus(String name, String engine) {
        this.name = name;
        this.engine = engine;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEngine() {
        return engine;
    }
}
