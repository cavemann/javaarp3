package com.sda.patterns.creational.abstractFactory;

public class FordFiesta extends Ford {
    private String name;
    private String engine;

    FordFiesta(String name, String engine) {
        this.name = name;
        this.engine = engine;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getEngine() {
        return engine;
    }
}
