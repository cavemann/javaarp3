package com.sda.patterns.creational.abstractFactory;

public abstract class Ford {
    public abstract String getName();
    public abstract String getEngine();

    @Override
    public String toString() {
        return "Name: " + getName() + ", engine: " + getEngine();
    }
}
