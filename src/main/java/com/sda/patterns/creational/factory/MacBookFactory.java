package com.sda.patterns.creational.factory;

import org.apache.log4j.Logger;

import java.util.NoSuchElementException;

public class MacBookFactory {
    private static final Logger LOGGER = Logger.getLogger(MacBookFactory.class);

    public static MacBook getMac(String type, String memory, String disc, int screenSize, String processor) {
        if(type.equalsIgnoreCase("Air")) {
            LOGGER.info("MacBook Air w produkcji");
            return new MacBookAir(memory, disc, screenSize);
        } else if (type.equalsIgnoreCase("Pro")) {
            LOGGER.info("MacBook Pro w produkcji z procesorem " + processor);
            return new MacBookPro(memory, disc, screenSize, processor);
        } else {
            throw new NoSuchElementException("Nieznany typ MacBooka");
        }
    }
}
