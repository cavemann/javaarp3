package com.sda.patterns.creational.factory;

public class FactoryDemo {
    public static void main(String[] args) {
        /**
         * Fabryka jest po to żeby nie pozwalać wszędzie na robienie "new"
         * czyli żeby tworzenie obiektów nie było udostępnione dla każdego, wszędzie i zawsze
         *
         * Lepiej jest zrobić dedykowaną metodę która przyjmie parametry
         * z jakimi obiekt ma być utworzony
         * I go wygeneruje (a może użyje istniejącego)
         */

        MacBook pro1 = MacBookFactory.getMac("Pro", "512G", "1TB", 16, "M1");
        MacBook pro2 = MacBookFactory.getMac("Pro", "256G", "512GB", 16, "Intel");
        MacBook air1 = MacBookFactory.getMac("Air", "128G", "512GB", 16, "M1");
        MacBook air2 = MacBookFactory.getMac("Air", "254G", "256GB", 13, "Intel");
        MacBook unknown = MacBookFactory.getMac("", "512G", "1TB", 16, "M1");

        MacBook pro3 = new MacBookPro("128", "256", 13, "Intel");

        /**
         * Fabryka jest użyteczna wtedy kiedy mamy rodzinę obiektów podobnych do siebie, z identycznymi
         * lub bardzo podobnymi konstruktorami
         */
    }
}
