package com.sda.patterns.creational.factory;

public class MacBookPro extends MacBook {
    private String memory;
    private String disk;
    private int screenSize;
    private String processor;

    MacBookPro(String memory, String disk, int screenSize, String processor) {
        this.memory = memory;
        this.disk = disk;
        this.screenSize = screenSize;
        this.processor = processor;
    }

    @Override
    public String getMemory() {
        return null;
    }

    @Override
    public String getDisk() {
        return null;
    }

    @Override
    public int getScreenSize() {
        return 0;
    }

    public String getProcessor() {
        return processor;
    }
}
