package com.sda.patterns.creational.factory;

public abstract class MacBook {
    public abstract String getMemory();
    public abstract String getDisk();
    public abstract int getScreenSize();

    //Metoda z ciałem w klasie abstrakcyjnej
    public String getMacDetails() {
        return "Memory: " + getMemory() + ", disk: " + getDisk() + ", screen size: " + getScreenSize();
    }

    @Override
    public String toString() {
        return "Memory: " + getMemory() + ", disk: " + getDisk() + ", screen size: " + getScreenSize();
    }
}
