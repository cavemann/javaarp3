package com.sda.streams;

import java.util.Comparator;

public class HumanByNameSurnameAgeComparator implements Comparator<Human> {
    @Override
    public int compare(Human human1, Human human2) {
        if(human1.getName().compareTo(human2.getName()) != 0) {
            return human1.getName().compareTo(human2.getName());
        }
        if(human1.getSurname().compareTo(human2.getSurname()) != 0) {
            return human1.getSurname().compareTo(human2.getSurname());
        }
        return human1.getAge() - human2.getAge();
    }
}
