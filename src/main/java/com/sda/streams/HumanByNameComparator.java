package com.sda.streams;

import java.util.Comparator;

public class HumanByNameComparator implements Comparator<Human> {

    @Override
    public int compare(Human human1, Human human2) {
        return human1.getName().compareTo(human2.getName());
    }
}
