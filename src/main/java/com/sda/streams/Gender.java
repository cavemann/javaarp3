package com.sda.streams;

public enum Gender {
    WOMAN(1, "k", "w", "kobieta"),
    MAN(2, "m", "m", "mężczyzna");

    private int index;
    private String shortcutPL;
    private String shortcutEN;
    private String translationPL;

    Gender(int index, String shortcutPL, String shortcutEN, String translationPL) {
        this.index = index;
        this.shortcutPL = shortcutPL;
        this.shortcutEN = shortcutEN;
        this.translationPL = translationPL;
    }

    public static Gender getByShortcutPL(String genderShortcutPl) {
        for(Gender gender : Gender.values()) {
            if (genderShortcutPl.equalsIgnoreCase(gender.getShortcutPL())) {
                return gender;
            }
        }
        return null;
    }

    public int getIndex() {
        return index;
    }

    public String getShortcutPL() {
        return shortcutPL;
    }

    public String getShortcutEN() {
        return shortcutEN;
    }

    public String getTranslationPL() {
        return translationPL;
    }
}
