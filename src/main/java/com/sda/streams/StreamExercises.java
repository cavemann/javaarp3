package com.sda.streams;

import org.apache.log4j.Logger;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Zasada działania strumienia to Pantha Rei
 * czyli "wszystko płynie" czyli
 * nie da się drugi raz przejść przez ten sam strumień
 *
 * Metody działające na strumieniach dzielimy na 3 grupy:
 * 1. Tworzące strumień - musi wystąpić jedna
 *   - stream() - tworząca strumień z kolekcji
 *   - of() - na elementach wyliczanych (np pojedyncze obiekty)
 *   - Arrays.stream() - tworząca strumień z tabeli
 *   - na danych z pliku
 *   - ... w kolejnych Javach coś się jeszcze może pojawiać
 * 2. Pośrednie - wykonujące działania na elementach strumienia - może nie wydtąpić żadna, może wystąpić kilka
 *   - filter - filtruje elementy strumienia wg zadanego kryterium
 *   - limit - ogranicza ilość elementów które przechodzą dalej
 *   - sorted - pozwala na sortowanie elementów strumienia, komparatorem domyślnym lub zewnętrznym
 *   - map - przekształca obiekt na inny wykonując jakąś akcję, w szczególności może przekształcić obiekt
 *           na taki sam obiekt, ale np. z uzupełnionym / zmienionym jednym z pól
 *   - flatmap - przyjmuje strumień kolekcji, strumień elementów tej kolekcji (spłaszcza bo
 *           wyciąga elementy z kolekcji)
 *   - distinct - wybiera elementy niepowtarzalne
 * 3. Końcowe - musi wystąpić jedna, inaczej całość kodu strumieniowego się nie wykona
 *   - forEach - wykonuje akcję na wszystkich otrzymanych elementach strumienia, nic nie zwraca (przyjmuje Consumenra)
 *   - collect - zbiera elementy na wyjściu strumienia i przekształca na postać końcową
 *               (kolekcja albo pojedyncza wartość)
 *   - sum - wylicza sumę podanych wartości
 *   - max - podaje maksymalną wartość ze strumienia (jest Optionalem, wymaga dodania orElse, albo zadeklarowania Optional)
 */
public class StreamExercises {
    private static final Logger LOGGER = Logger.getLogger(StreamExercises.class);

    public static void main(String[] args) {

        /**
         * DTO czyli Data Transfer Object
         * czyli obiekt który stanowi wycinek obiektu bazowego
         * albo złożenie wycinków kilku obiektów
         * Robi się to żeby do innego systemu, albo do innej części własnego systemu
         * przekazać dane ale tylko te które są tam niezbędne, nie trzeba całego "oryginalnego" obiektu
         */
        class HumanDTO {
            String name;
            String surname;

            public HumanDTO(String name, String surname) {
                this.name = name;
                this.surname = surname;
            }

            public String getHumanDtoData() {
                return surname + " " + name;
            }
        }

        List<Human> people = new ArrayList<>();
        people.add(new Human("Marian", "Kowalski", 33, Gender.MAN));
        people.add(new Human("Wojciech", "Nowak", 38, Gender.MAN));
        people.add(new Human("Lucyna", "Pięta", 42, Gender.WOMAN));

        people.add(new Human("Katarzyna", "Kowalski", 23, "k"));
        people.add(new Human("Lidia", "Zawistowska", 35, "k"));
        people.add(new Human("Arkadiusz", "Rycki", 58, "m"));

        LOGGER.info("=====================================================");
        LOGGER.info("Ludzie powyżej 33 lat");
        people.stream()
                .filter(x -> x.getAge() > 33)
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Mężczyźni powyżej 33 lat");
        people.stream()
                .filter(x -> x.getAge() > 33)
                .filter(x -> Gender.MAN == x.getGender())
                //.filter(x -> x.getAge() > 33 && Gender.MAN == x.getGender())
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Dwa pierwsze wyniki");
        people.stream()
                .limit(2)
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Kolejność ma znaczenie: ");
        LOGGER.info("Najpierw limit, potem filter");
        people.stream()
                .limit(2)
                .filter(x -> Gender.WOMAN == x.getGender())
                .forEach(LOGGER::info);
        LOGGER.info("Najpierw filter, potem limit");
        people.stream()
                .filter(x -> Gender.WOMAN == x.getGender())
                .limit(2)
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Sortowanie komparatorem domyślnym: ");
        people.stream()
                .sorted()
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Sortowanie komparatorem zewnętrznym: ");
        people.stream()
                .sorted(new HumanByNameComparator())
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Sortowanie po wieku komparatorem z klasy anonimowej, bez klasy zewnętrznej: ");
        people.stream()
                .sorted(Comparator.comparing(Human::getAge))
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Tworzymy nową listę na której będą tylko kobiety");
        List<Human> womenOnly = people.stream()
                .filter(x -> Gender.WOMAN == x.getGender())
                .collect(Collectors.toList());
        LOGGER.info("=====================================================");
        LOGGER.info("Wypiszemy tylko imię i nazwisko");
        people.stream()
                .map(x -> x.getName() + " " + x.getSurname())
                .forEach(LOGGER::info);
        LOGGER.info("=====================================================");
        LOGGER.info("Wyciągniemy wszystkich mężczyzn i wstawimy ich do seta");
        Set<Human> manOnly = people.stream()
                .filter(x -> Gender.MAN == x.getGender())
                .collect(Collectors.toSet());
        LOGGER.info("=====================================================");
        LOGGER.info("Złączymy manOnly i womenOnly i ich elementy przekształcimy do DTO");
        List<HumanDTO> dtos = Set.of(manOnly, womenOnly).stream()
                .flatMap(Collection::stream)
                .map(each -> new HumanDTO(each.getName(), each.getSurname()))
                .collect(Collectors.toList());
        LOGGER.info("=====================================================");
        LOGGER.info("Wypiszemy ciągiem wszystkie imiona");
        String resultJoined = dtos.stream()
                .map(each -> each.name)
                .collect(Collectors.joining());
        LOGGER.info(resultJoined);
        LOGGER.info("=====================================================");
        LOGGER.info("Wypiszemy ciągiem wszystkie nazwiska, z przecinkami między nimi");
        resultJoined = dtos.stream()
                .map(each -> each.surname)
                .collect(Collectors.joining(", "));
        LOGGER.info(resultJoined);
        LOGGER.info("=====================================================");
        LOGGER.info("Wypiszemy ciągiem wszystkie nazwiska, bez powtórzeń, z przecinkami między nimi, z nawiasami na początku i końcu");
        resultJoined = dtos.stream()
                .map(each -> each.surname)
                .distinct()
                .collect(Collectors.joining("', '", "{'", "'}"));
        LOGGER.info(resultJoined);
        LOGGER.info("=====================================================");
        LOGGER.info("Z listy zrobimy mapę gdzie kluczem jest imię a wartością obiekt Human");
        Map<String, Human> peopleByName = people.stream()
                .collect(Collectors.toMap(Human::getName, x -> x));
        LOGGER.info("Drukujemy zawartość mapy:");
        peopleByName.entrySet().forEach(x -> {
            LOGGER.info("Klucz: " + x.getKey());
            LOGGER.info("Wartość: " + x.getValue());
        });
        LOGGER.info("=====================================================");
        LOGGER.info("Z listy zrobimy mapę gdzie kluczem jest nazwisko a wartością lista obiektów Human");
        Map<String, List<Human>> peopleBySurname = people.stream()
                .collect(Collectors.groupingBy(Human::getSurname, Collectors.toList()));
        LOGGER.info("Drukujemy zawartość mapy:");
        peopleBySurname.entrySet().forEach(x -> {
            LOGGER.info("Klucz: " + x.getKey());
            LOGGER.info("Wartość: " + x.getValue());
        });

        LOGGER.info("=====================================================");
        LOGGER.info("Zsumujmy wiek ludzi:");
        int ageSummed = people.stream()
                .mapToInt(Human::getAge)
                .sum();
        LOGGER.info("Suma lat: " + ageSummed);
        LOGGER.info("=====================================================");
        LOGGER.info("Najwyższy wiek:");
        int maxAge = people.stream()
                .mapToInt(Human::getAge)
                .max()
                .orElse(0);
        LOGGER.info("Wiek: " + maxAge);
        LOGGER.info("=====================================================");
        LOGGER.info("Suma lat mężczyzn (reduce a nie sum) :");
        int sumManAge = people.stream()
                .map(Human::getAge)
                .reduce(0, (current, incoming) -> current + incoming);
        LOGGER.info(sumManAge);
        LOGGER.info("=====================================================");
        LOGGER.info("Set Kowalskich wyciągnięta z mapy gdzie kluczem było nazwisko :");
        Set<Human> kowalscy = peopleBySurname.entrySet()
                .stream()
                .filter(mapEntry -> "Kowalski".equals(mapEntry.getKey()))
                .map(Map.Entry::getValue)
                .findFirst()
                .stream()
                .flatMap(Collection::stream)
                .collect(Collectors.toSet());
        LOGGER.info(kowalscy);

        /**
         * Map<K, V>
         * czyli mapa zestawów klucz wartość
         * możliwe zestawy do wyciągnięcia:
         *  - klucze (keySet()) - zestaw kluczy danej mapy, u nas imiona lub nazwiska
         *  - wartości (values()) - zestaw wartości danej mapy, u nas ludzie lub listy ludzi
         *  - wpisy (entrySet()) - zestaw par klucz-wartość, można się dobrać do klucza entry.key
         *                         albo do wartości entry.value
         */

        LOGGER.info("==========================================================");
        LOGGER.info("Klucze z mapy wyciągamy przez keySet() ");
        for(String name: peopleByName.keySet()) {
            LOGGER.info("Drukujemy klucz: " + name);
            LOGGER.info("Drukujemy wartość dla klucza: " + peopleByName.get(name));
        }
        LOGGER.info("==========================================================");
        LOGGER.info("Wartości z mapy wyciągamy przez values()");
        LOGGER.info("Przy wyciąganiu przez wartości nie ma możliwości pobrania klucza");
        for(Human human : peopleByName.values()) {
            LOGGER.info("Drukujemy wartość : " + human);
        }
        LOGGER.info("Dodaję drugi raz tego samego Humana ale pod inny klucz");
        peopleByName.put("Nowy", peopleByName.get("Arkadiusz"));
        for(Human human : peopleByName.values()) {
            LOGGER.info("Drukujemy wartość : " + human);
        }
        LOGGER.info("==========================================================");
        LOGGER.info("Wpisy wyciągane z mapy przez entrySet()");
        for(Map.Entry entry : peopleByName.entrySet()) {
            LOGGER.info("Klucz wpisu: " + entry.getKey());
            LOGGER.info("Wartość wpisu: " + entry.getValue());
        }
    }
}
