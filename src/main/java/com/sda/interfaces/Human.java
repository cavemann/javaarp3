package com.sda.interfaces;

import org.apache.log4j.Logger;

public class Human implements Movable {
    private static final Logger LOGGER = Logger.getLogger(Human.class);

    private String name;
    private String surname;

    public Human(String name, String surname) {
        this.name = name;
        this.surname = surname;
    }

    @Override
    public void move() {
        LOGGER.info(name + " " + surname + " idzie!");
    }

    @Override
    public void moveFast() {
        LOGGER.info(name + " " + surname + " biegnie!");
    }

    //nadpisana metoda domyślna z interfejsu
    @Override
    public int calcDistanceMoved(String source, String dest) {
        LOGGER.info("Liczymy dystand od " + source + " do " + dest + " ale po łuku!");
        return 100;
    }
}
