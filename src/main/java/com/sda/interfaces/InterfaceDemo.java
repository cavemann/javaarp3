package com.sda.interfaces;

public class InterfaceDemo {

    public static void main(String[] args) {

        Bird bird1 = new Bird("Wróbel");
        Human human1 = new Human("Marcin", "Nowak");
        bird1.move();
        bird1.moveFast();
        human1.move();
        human1.moveFast();
        bird1.calcDistanceMoved("Kraków", "Niepołomice");
        human1.calcDistanceMoved("Warszawa", "Łomianki");
    }
}
