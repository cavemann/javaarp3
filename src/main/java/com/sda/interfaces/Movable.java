package com.sda.interfaces;

//@FunctionalInterface - tylko jeśli mamy dokładnie jedną metodę abstrakcyjną
public interface Movable {
    //metoda w interfejsie domyślnie jest public abstract, nie trzeba tego pisać
    void move();

    //metod abstrakcyjnych może być kilka, o ile nie określimy interfejsu jako funkcyjnego
    void moveFast();

    //interfejs może zawierać metody domyślne
    default int calcDistanceMoved(String source, String dest) {
        System.out.println("Jakoś liczymy odległość od " + source + " do " + dest);
        return Math.abs(source.length() - dest.length());
    }

    //może zawierać metody prywatne
    private int somePrivateMethod() {
        return 4;
    }

    //może zawierać metody statyczne
    static int someStaticMethod() {
        return 8;
    }
}
