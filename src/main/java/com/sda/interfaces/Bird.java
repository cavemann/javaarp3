package com.sda.interfaces;

import org.apache.log4j.Logger;

/**
 * W klasie Bird nie nadpisujemy metody defaultowej z interfejsu
 */
public class Bird implements Movable {
    private static final Logger LOGGER = Logger.getLogger(Bird.class);
    private String name;

    public Bird(String name) {
        LOGGER.info("Tworzymy Birda");
        this.name = name;
    }

    @Override
    public void move() {
        LOGGER.info(name + " lata!");
    }

    @Override
    public void moveFast() {
        LOGGER.info(name + " lata bardzo szybko!!");
    }
}
