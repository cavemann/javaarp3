package com.sda.functionalInterfaces.supplier;

import com.sda.functionalInterfaces.Car;
import org.apache.log4j.Logger;

import java.util.function.Supplier;

/**
 * Supplier jest interfejsem funkcyjnym "dostawczym"
 * Nie przyjmuje żadnych argumentów, zwraca obiekt
 *
 * Definiując Suppliera określamy implementację jego metody get
 */

public class SupplierDemo {

    private final static Logger LOGGER = Logger.getLogger(SupplierDemo.class);

    public static void main(String[] args) {
        Car familyCar = new Car("Ford CMax", "1.6");

        Supplier<Car> sportCar = () -> new Car("Ford Mustang GT", "3.5");
        Supplier<Car> truckCar = () -> {
            LOGGER.info("Creating truck car from supplier");
            return new Car("VW Transporter", "2.2");
        };

        LOGGER.info(sportCar.get());
        LOGGER.info("Info o samochodzie dostawczym");
        LOGGER.info(carInfo(truckCar));
    }

    public static String carInfo(Supplier<Car> carSupply) {
        Car passedCar = carSupply.get();
        return new StringBuilder(passedCar.getName())
                .append(", ")
                .append(passedCar.getEngine())
                .toString();
    }

}
