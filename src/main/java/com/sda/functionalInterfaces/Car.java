package com.sda.functionalInterfaces;

import org.apache.log4j.Logger;

public class Car {
    private static final Logger LOGGER = Logger.getLogger(Car.class);

    private String name;
    private String engine;

    public Car(String name, String engine) {
        LOGGER.info("Tworzymy nowy samochód: " + name + " z silnikiem: " + engine);
        this.name = name;
        this.engine = engine;
    }

    public String getName() {
        return name;
    }

    public String getEngine() {
        return engine;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", engine='" + engine + '\'' +
                '}';
    }
}
