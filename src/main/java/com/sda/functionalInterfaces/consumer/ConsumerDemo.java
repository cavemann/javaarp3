package com.sda.functionalInterfaces.consumer;

import com.sda.functionalInterfaces.Car;
import org.apache.log4j.Logger;

import java.util.List;
import java.util.function.Consumer;

/**
 * Consumer - interfejs funkcyjny przyjmujący obiekt i nie zwracający nic
 *
 * Przy definiowaniu interfejsu Consumer nadpisujemy jego metodę accept
 */

public class ConsumerDemo {
    private static final Logger LOGGER = Logger.getLogger(ConsumerDemo.class);

    public static void main(String[] args) {
        Car sportCar = new Car("Porche 911", "4.2");
        Car familyCar = new Car("Volvo V40", "1.7");

        Consumer<Car> printCarMark = car -> {
            LOGGER.info("Printing car's mark:");
            LOGGER.info(car.getName().substring(0, car.getName().indexOf(" ")));
        };

        Consumer<Car> printCarInfoNoSpaces = car -> {
            LOGGER.info("No spaces: " + car.getName().replaceAll(" ", "") +
                    car.getEngine().replaceAll(" ", ""));
        };

        printCarMark.accept(sportCar);
        printCarMark.accept(familyCar);
        printCarInfoNoSpaces.accept(familyCar);

        System.out.println("sout sam jest Consumerem bo przyjmuje String a nic nie zwraca");

    }
}
