package com.sda.functionalInterfaces.optional;

import com.sda.functionalInterfaces.Car;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.function.Supplier;

public class OptionalDemo {

    private static final Logger LOGGER = Logger.getLogger(OptionalDemo.class);

    public static void main(String[] args) {
        Car familyCar = new Car("Ford SMax", "2.5");
        Car sportCar = new Car("Ford Mustang", "4.5");
        Car volvoCar = new Car("Volvo V50", "1.8");
        Car emptyCar = null;

        /**
         * Optional to kontener (pudełko, skrzynka) na obiekt
         * Ten obiekt może być w środku albo nie, możemy mieć pusty pudełko
         */

        //pusty kontener na obiekt:
        Optional<Car> emptyCarOptional = Optional.empty();

        //Kontener z zawartością:
        //metoda .of zakłada że zawartość występuje, jeśli nie to NPE
        Optional<Car> notEmptyCar = Optional.of(familyCar);

        //Kontener niekoniecznie z zawartością
        //w środku może nic nie być i nie skończy się NPE
        Optional<Car> possiblyEmptyCar = Optional.ofNullable(emptyCar);

        //pre-optional
        if(volvoCar == null) {
            //coś tam coś tam
        }

        //z optionalem
        //isPresent testuje czy coś jest w środku (od Javy 1.8)
        if(notEmptyCar.isPresent()) {
            Car car = notEmptyCar.get(); //wyciąganie zawartości z optionala
            LOGGER.info("W optionalu notEmptyCar coś jest");
        }

        //isEmpty testuje czy zawartość optionala jest pusta - od Javy 11 (chyba)
        if(emptyCarOptional.isEmpty()) {
            LOGGER.info("EmptyCarOptional nie zawiera samochodu");
        }

        List<Car> cars = Arrays.asList(familyCar, sportCar, volvoCar);

        //Optional z listy samochodów
        //po .filter może będzie jakiś wynik a może nie, stąd findFirst zwraca optionala
        Optional<Car> hugeEngineCar = cars.stream()
                .filter(each -> each.getEngine().compareTo("5.0") > 0)
                .findFirst();

        LOGGER.info("====================================================================");
        LOGGER.info("====================================================================");
        Car smallEngineCar = cars.stream()
                .filter(each -> each.getEngine().compareTo("5.0") < 0)
                .findFirst()
                .orElse(new Car("OrElseCar", "OrElseEngine"));
        LOGGER.info("Small engine car details: " +smallEngineCar.getName() + " " + smallEngineCar.getEngine());
        LOGGER.info("====================================================================");
        Car anotherSmallEngineCar = cars.stream()
                .filter(each -> each.getEngine().compareTo("5.0") < 0)
                .findAny()
                //.orElseGet(() -> new Car("OrElseGetCar", "OrElseGetEngine"));
                .orElseGet(OptionalDemo.getCarIfNecessary());
        LOGGER.info("Another small engine car details: " +anotherSmallEngineCar.getName() + " " + anotherSmallEngineCar.getEngine());
        LOGGER.info("====================================================================");
        LOGGER.info("====================================================================");
        Car yetAnotherSmallEngineCar = cars.stream()
                .filter(each -> each.getEngine().compareTo("5.0") < 0)
                .findAny()
                .orElseThrow();
    }

    private static Supplier<Car> getCarIfNecessary() {
        return () -> new Car("OrElseGetCar", "OrElseGetEngine");
    }
}
