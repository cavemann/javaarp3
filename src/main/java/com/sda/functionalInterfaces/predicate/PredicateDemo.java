package com.sda.functionalInterfaces.predicate;

import com.sda.functionalInterfaces.Car;
import org.apache.log4j.Logger;

import java.util.function.Predicate;

/**
 * Predicate to interfejs funkcyjny służący do testowania warunków
 * Jako parametr przyjmuje obiekt
 * Zwraca boolean (true / false)
 *
 * Definiując interfejs Predicate ZAWSZE nadpisujemy (definiujemy) metodę test()
 */

public class PredicateDemo {
    private static final Logger LOGGER = Logger.getLogger(PredicateDemo.class);

    public static void main(String[] args) {
        Car familyCar = new Car("Ford SMax", "2.5");
        Car sportCar = new Car("Ford Mustang GT", "4.5");
        Car volvoCar = new Car("Volvo V50", "2.0");

        Predicate<Car> isSmallEngine = x -> x.getEngine().compareTo("2.0") <= 0;
        Predicate<Car> isVolvo = check -> check.getName().startsWith("Volvo");

        if(isSmallEngine.test(sportCar)) {
            LOGGER.info(sportCar.getName() + " ma mały silnik: " + sportCar.getEngine());
        } else {
            LOGGER.info(sportCar.getName() + " ma duży silnik: " + sportCar.getEngine());
        }

        LOGGER.info("===========================================================");
        printCar(familyCar, isSmallEngine);
        LOGGER.info("===========================================================");
        printCar(familyCar, isVolvo);
        LOGGER.info("===========================================================");
        printCar(volvoCar, isSmallEngine);
        LOGGER.info("===========================================================");
        printCar(volvoCar, isVolvo);
        LOGGER.info("===========================================================");
    }

    private static void printCar(Car car, Predicate<Car> predicate) {
        if(predicate.test(car)) {
            LOGGER.info("Wyświetlamy auto bo spełnia test: " + car);
        }
    }
}
