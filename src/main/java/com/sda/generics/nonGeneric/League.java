package com.sda.generics.nonGeneric;

import com.sda.generics.Team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class League {
    private String leagueName;
    private List<Team> teams;

    public League(String leagueName) {
        this.leagueName = leagueName;
        teams = new ArrayList<>();
    }

    public boolean addTeam(Team team) {
        if(!teams.contains(team)) {
            return teams.add(team);
        }
        return false;
    }

    public String printTable() {
        StringBuilder table = new StringBuilder("League ")
                .append(leagueName);
        Collections.sort(teams);
        for(Team team : teams) {
            table.append("\n")
                    .append(team.getName())
                    .append(" ")
                    .append(team.getPoints());
        }
        return table.toString();
    }
}
