package com.sda.generics;

public class FootballTeam extends Team {
    private int noOfPlayers;

    public FootballTeam(String name, int noOfPlayers) {
        super(name);
        this.noOfPlayers = noOfPlayers;
    }

}
