package com.sda.generics.fullGeneric;

import com.sda.generics.Club;
import com.sda.generics.FootballTeam;
import com.sda.generics.Team;
import com.sda.generics.TennisClub;

public class LeagueMain {

    public static void main(String[] args) {
        League league = new League("Primiera Division");

        Team team1 = new FootballTeam("FC Barcelona", 35);
        Team team2 = new FootballTeam("Real Madrid CF", 33);
        Team team3 = new FootballTeam("Atletico de Madrid", 30);
        Team team4 = new FootballTeam("Valencia CF", 38);

        team1.setPoints(20);
        team2.setPoints(15);
        team3.setPoints(26);
        team4.setPoints(23);

        league.addTeam(team1);
        league.addTeam(team2);
        league.addTeam(team3);
        league.addTeam(team4);

        System.out.println(league.printTable());

        System.out.println("=================================");

        Club club1 = new TennisClub("Warszawski klub tenisa");
        Club club2 = new TennisClub("Wrocławski klub tenisa");
        club1.setPoints(23);
        club2.setPoints(38);
        League tennisLeague = new League("Liga tenisa ziemnego");
        tennisLeague.addTeam(club1);
        tennisLeague.addTeam(club2);
        System.out.println(tennisLeague.printTable());
        /**
         * Poniżej przepis jak zepsuć sobie wykorzystanie generyków
         */
        tennisLeague.addTeam(team1);
        System.out.println(tennisLeague.printTable());
    }
}
