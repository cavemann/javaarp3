package com.sda.generics.fullGeneric;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @param <T> - "jakaś" klasa, bez określania jaka ona jest, w tym przypadku może być
 *           zupełnie dowolna
 *           Wszędzie tam gdzie robimy operacje na liście "teams", bądź jej elementach,
 *           wskazujemy że działąmy na typie uogólnionym T
 */
public class League<T> {
    private String leagueName;
    private List<T> teams;

    public League(String leagueName) {
        this.leagueName = leagueName;
        teams = new ArrayList<>();
    }

    public boolean addTeam(T team) {
        if(!teams.contains(team)) {
            return teams.add(team);
        }
        return false;
    }

    /**
     * Drukując tabelkę typu uogólnionego
     * nie możemy zrobić sortowania bo nie wiemy
     * czy konkretna klasa która tu będzie wywołana
     * implementuje Comparable
     * Nie możemy też sięgnąć do szczegółów tej klasy (pól)
     * bo nie wiemy jakie one są - tu może być wszystko, dowolna klasa
     */
    public String printTable() {
        StringBuilder table = new StringBuilder("League ")
                .append(leagueName);
        for(T team : teams) {
            table.append("\n")
                 .append(team);
        }
        return table.toString();
    }
}
