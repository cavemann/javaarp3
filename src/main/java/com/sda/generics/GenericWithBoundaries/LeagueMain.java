package com.sda.generics.GenericWithBoundaries;

import com.sda.generics.*;

public class LeagueMain {

    public static void main(String[] args) {
        League<FootballTeam> league = new League("Primiera Division");

        FootballTeam team1 = new FootballTeam("FC Barcelona", 35);
        FootballTeam team2 = new FootballTeam("Real Madrid CF", 33);
        FootballTeam team3 = new FootballTeam("Atletico de Madrid", 30);
        FootballTeam team4 = new FootballTeam("Valencia CF", 38);

        team1.setPoints(20);
        team2.setPoints(15);
        team3.setPoints(26);
        team4.setPoints(23);

        league.addTeam(team1);
        league.addTeam(team2);
        league.addTeam(team3);
        league.addTeam(team4);

        System.out.println(league.printTable());

        System.out.println("=================================");
        VolleyballTeam volleyballTeam1 = new VolleyballTeam("Asseco Resovia");
         //league.addTeam(VolleyballTeam); nie zadziała, wywala się przed kompilacją
        //league.addTeam((FootballTeam) volleyballTeam1); też wywala się przed kompilacją
        Team volleyballTeam2 = new VolleyballTeam("Trefl Sopot");
        //league.addTeam((FootballTeam) volleyballTeam2); //kompiluje się ale wywala w działąniu

    }
}
