package com.sda.generics.GenericWithBoundaries;

import com.sda.generics.Team;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * <T extends Team> oznacza klasę "jakąś" dziedziczącą po Team
 *  - rezygnujemy z całkowitej dowolności - decydujemy że klasa League
 *    będzie tylko dla "rodziny klas" Team
 *  - ale zyskujemy możliwość skorzystania z metod charakterystycznych
 *    dla Team (sortowanie, pobieranie pól) bo wiemy że dowolna klasa
 *    dziedzicząca po Team nam to umożliwi
 */
public class League<T extends Team> {
    private String leagueName;
    private List<T> teams;

    public League(String leagueName) {
        this.leagueName = leagueName;
        teams = new ArrayList<>();
    }

    public boolean addTeam(T team) {
        if(!teams.contains(team)) {
            return teams.add(team);
        }
        return false;
    }

    public String printTable() {
        StringBuilder table = new StringBuilder("League ")
                .append(leagueName);
        Collections.sort(teams);
        for(T team : teams) {
            table.append("\n")
                    .append(team.getName())
                    .append(" ")
                    .append(team.getPoints());
        }
        return table.toString();
    }
}
