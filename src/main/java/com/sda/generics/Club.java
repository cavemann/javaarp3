package com.sda.generics;

public abstract class Club {
    private String clubName;
    private int points;

    public Club(String clubName) {
        this.clubName = clubName;
    }

    public String getClubName() {
        return clubName;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return clubName + " " + points;
    }
}
