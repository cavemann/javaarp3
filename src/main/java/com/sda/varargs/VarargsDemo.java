package com.sda.varargs;

/**
 * Varargs czyli zestaw parametrów tego samego typu, ale o nieokreślonej ilości
 */

public class VarargsDemo {


    public static void main(String[] args) {
        VarargsDemo demo = new VarargsDemo();
        System.out.println(demo.sumAllPassedInts(1));
        System.out.println(demo.sumAllPassedInts());
        System.out.println(demo.sumAllPassedInts(1, 2, 3, 5, 6, 7));

        System.out.println(demo.sumOneRequiredAndVarargs(3));
        System.out.println(demo.sumOneRequiredAndVarargs(4, 5));
        System.out.println(demo.sumOneRequiredAndVarargs(9, 2, 2, 2, 2, 2, 2, 2, 2));
    }

    /**
     * Varargs pozwala przekazać od 0 do n wartości
     */
    private int sumAllPassedInts(int... values) {
        int result = 0;
        for (int value : values) {
            result += value;
        }
        return result;
    }

    /**
     * Można do metody przekazać x elementów wymaganych i varargs - wtedy varargs jako ostatni parametr
     * i może być tylko 1 parametr typu varargs
     */
    private int sumOneRequiredAndVarargs(int a, int... values) {
        return a + sumAllPassedInts(values);
    }

}
