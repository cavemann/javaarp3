package com.sda.lambdas;

import org.apache.log4j.Logger;

public class LambdaSamples {
    private final static Logger LOGGER = Logger.getLogger(LambdaSamples.class);

    public static void main(String[] args) {
        /**
         * Lambda (wyrażenie lambda lub funkcja lambda)
         * postać: () -> {}
         * gdzie
         *  () - może ale nie musi zawierać parametry (jeden lub więcej)
         *       można zapisać samą listę argumentów, czyli nawiasy nie są wymagane
         *       ale jeśli parametrów nie ma to trzeba je dać - na potrzeby czytelności kodu i kompilatora
         *  -> operator pomiędzy parametrami a ciałem lambdy
         *  {} - zawiera kod do wykonania, jeśli jest jednolinijkowy to same
         *       nawiasy klamrowe nie są wymagane
         */
        String first = "aaa";
        String second = "bbb";

        /**
         * Robimy implementację interfejsu StringAdding
         * wyrażenie poniżej odnosi się do metody abstrakcyjnej z interfejsu
         */
        StringAdding adding = (a, b) -> a + b;

        StringAdding addingWithTypes = (String a, String b) -> a + b;

        //nawiasy klamrowe wymuszają klauzuję return i średnik na końcu wyrażenie
        StringAdding addingWithBrackets = (a, b) -> {return a + b;};

        //wieloliniowa lambda
        StringAdding multiLineAdding = (a, b) -> {
            LOGGER.info("Wewnątrz wieloliniowej lambdy");
            return a.substring(0,1) + b.substring(0,1);
        };

        LOGGER.info("==================================================================");
        LOGGER.info(adding.add(first, second));
        LOGGER.info("==================================================================");
        LOGGER.info(addingWithTypes.add(first, second));
        LOGGER.info("==================================================================");
        LOGGER.info(addingWithBrackets.add(first, second));
        LOGGER.info("==================================================================");
        LOGGER.info(multiLineAdding.add(first, second));
        LOGGER.info("==================================================================");

        /**
         * Rozwlekła, oparata na anonimowej implementacji
         * klasy abstrakcyjnej, postać interfejsu StringAdding
         * Klasa anonimowa bo nie nadajemy nazwy (nie mamy struktury class XXX implements StringAdding)
         */
        StringAdding abstractClassStyle = new StringAdding() {
            @Override
            public String add(String arg1, String arg2) {
                return arg2 + arg1;
            }
        };

        LOGGER.info(abstractClassStyle.add(first, second));
        LOGGER.info("==================================================================");

    }
}
