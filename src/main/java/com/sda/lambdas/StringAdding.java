package com.sda.lambdas;

/**
 * @FunctionalInterface oznacza że mamy DOKŁADNIE jedną metodę abstrakcyjną w interfejsie
 */
@FunctionalInterface
public interface StringAdding {
    String add(String arg1, String arg2);
}
