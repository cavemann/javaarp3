package com.sda.lambdas;

/**
 * Na StringPerforming nie możemy nałożyć @FunctionalInterface bo
 * oprócz własnej metody abstrakcyjnej intersect
 * ma też drugą (add) - dziedziczoną po StringAdding
 */
//@FunctionalInterface
public interface StringPerforming extends StringAdding {
    String intersect(String arg1, String arg2, int intersect);
}
