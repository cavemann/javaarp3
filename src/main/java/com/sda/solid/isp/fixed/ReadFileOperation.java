package com.sda.solid.isp.fixed;

public interface ReadFileOperation {
    byte[] read();
}
