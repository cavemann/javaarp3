package com.sda.solid.isp.fixed;

public interface FileOperation extends ReadFileOperation {
    void write(byte[] data);
}
