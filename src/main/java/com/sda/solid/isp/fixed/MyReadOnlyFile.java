package com.sda.solid.isp.fixed;

public class MyReadOnlyFile implements ReadFileOperation {
    @Override
    public byte[] read() {
        return new byte[0];
    }
}
