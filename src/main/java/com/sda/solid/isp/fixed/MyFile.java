package com.sda.solid.isp.fixed;

import org.apache.log4j.Logger;

public class MyFile implements FileOperation {

    private static final Logger LOGGER = Logger.getLogger(MyFile.class);

    @Override
    public void write(byte[] data) {
        LOGGER.info("Writing data to file");
    }

    @Override
    public byte[] read() {
        return new byte[0];
    }
}
