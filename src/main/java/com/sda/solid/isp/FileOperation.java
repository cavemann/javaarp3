package com.sda.solid.isp;

public interface FileOperation {
    byte[] read();

    void write(byte[] data);
}
