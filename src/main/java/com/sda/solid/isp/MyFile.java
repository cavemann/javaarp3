package com.sda.solid.isp;

import org.apache.log4j.Logger;

public class MyFile implements FileOperation {

    private static final Logger LOGGER = Logger.getLogger(MyFile.class);

    @Override
    public byte[] read() {
        return new byte[0];
    }

    @Override
    public void write(byte[] data) {
        LOGGER.info("Writing to file");
    }
}
