package com.sda.solid.isp;

/**
 * ISP - Interface Segregation Principle
 * czyli zasada segregacji interfejsów która mówi żeby podzielić interfejsy
 * na tak małe żeby nie zmuszać klas do implementacji metod interfejsu
 * które tej klasie nie są potrzebne a wręcz mogą wprowadzać w błąd
 */

public class MyReadOnlyFile implements FileOperation {
    @Override
    public byte[] read() {
        return new byte[0];
    }

    /**
     * Nie potrzebujemy w klasie do obsługi plików read-only metody
     * służącej do zapisywania danych do pliku
     */
    @Override
    public void write(byte[] data) {
        throw new UnsupportedOperationException("Can't write to read only file");
    }
}
