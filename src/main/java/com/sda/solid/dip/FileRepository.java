package com.sda.solid.dip;

import org.apache.log4j.Logger;

public class FileRepository {

    private static final Logger LOGGER = Logger.getLogger(FileRepository.class);

    public void saveTask(String filename) {
        LOGGER.info("Saving data to file: " + filename);
    }

    public void deleteTask(String filename) {
        LOGGER.info("Delete from file: " + filename);
    }
}
