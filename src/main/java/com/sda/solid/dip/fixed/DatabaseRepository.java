package com.sda.solid.dip.fixed;

import org.apache.log4j.Logger;

public class DatabaseRepository implements Repository {
    private static final Logger LOGGER = Logger.getLogger(DatabaseRepository.class);

    @Override
    public void saveTask(String task) {
        LOGGER.info("Saving task to database");
    }

    @Override
    public void deleteTask(String task) {
        LOGGER.info("Removing task from database");
    }
}
