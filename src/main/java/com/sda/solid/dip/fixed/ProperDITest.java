package com.sda.solid.dip.fixed;


import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class ProperDITest {
    private static final Logger LOGGER = Logger.getLogger(ProperDITest.class);

    public static void main(String[] args) {
        Repository repository = new FileRepository();
        TaskService service = new TaskService(repository);
        service.addTask("Task to add");
        service.removeTask("Task to remove");

        LOGGER.info("====================================================");
        LOGGER.info("Zmieniamy repository w TaskService na DatabaseRepository");
        service = new TaskService(new DatabaseRepository());
        service.addTask("Another task to add");
        service.removeTask("Other task to remove");

        LOGGER.info("====================================================");
        List<String> surnames = new ArrayList<>();
    }
}
