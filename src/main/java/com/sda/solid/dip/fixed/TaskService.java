package com.sda.solid.dip.fixed;

public class TaskService {

    /**
     * Pole typu interfejs i wrzucenie do pola wartości przez setter / konstruktor
     * powoduje powstanie tzw. loose couplingu czyli luźnego wiązania
     * Klasa nie jest już sztywno związana z inną klasą tylko raczej
     * lużno z interfejsem dla którego może istnieć kolka implementacji
     */
    Repository repository;

    public TaskService(Repository repository) {
        this.repository = repository;
    }

    public void addTask(String task) {
        repository.saveTask(task);
    }

    public void removeTask(String task) {
        repository.deleteTask(task);
    }
}
