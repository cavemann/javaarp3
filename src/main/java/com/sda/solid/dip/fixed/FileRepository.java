package com.sda.solid.dip.fixed;

import org.apache.log4j.Logger;

public class FileRepository implements Repository {

    private final static Logger LOGGER = Logger.getLogger(FileRepository.class);

    @Override
    public void saveTask(String task) {
        LOGGER.info("Saving task to file");
    }

    @Override
    public void deleteTask(String task) {
        LOGGER.info("Removing task to file");
    }
}
