package com.sda.solid.dip;

/**
 * High coupling czyli silne wiązanie
 * występuje np kiedy w jednej klasie mamy utworzyć obiekt innej klasy
 * a nie dostajemy go z zewnątrz (konstruktorem, setterem, do pola)
 */

public class TaskService {
    private FileRepository repository = new FileRepository();

    public void addTask(String task) {
        repository.saveTask(task);
    }

    public void deleteTask(String task) {
        repository.deleteTask(task);
    }
}
