package com.sda.solid.dip;

public class FailedDipDemo {
    public static void main(String[] args) {
        TaskService service = new TaskService();
        service.addTask("SampleFile.txt");
        service.deleteTask("OtherFile.txt");
    }
}
