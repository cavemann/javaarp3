package com.sda.io.serialization;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SerializationApp {

    /**
     * Serializacja, to zamiana obiektu na strumień danych, który następnie jest
     * zapisywany do pliku, wysyłany siecią itp.
     *
     * Deserializacja to proces odwrotny, z strumienia danych wyciągamy obiekt
     */

    public static void main(String[] args) {
        SerializedPerson person = new SerializedPerson(1, "Antoni", "Lipski", 33);

        try {
            FileOutputStream fout = new FileOutputStream("Serial.data");
            ObjectOutputStream oout = new ObjectOutputStream(fout);

            oout.writeObject(person);

            oout.close();
            fout.close();
        } catch (FileNotFoundException e) {
            System.out.println("Plik nie istnieje");
        } catch (IOException e) {
            System.out.println("Problem z zapisem do pliku");
        }


    }
}
