package com.sda.io.serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;

public class DeserializationApp {

    /**
     * Deserializacja wywali się jeśli mamy obiekty w pliku (lub innym zasobie)
     * i przed przekształceniem ich w obiekt zmienimy klasę tego obiektu - przez dodanie
     * lub usunięcie lub modyfikację pola bądź metody
     */

    public static void main(String[] args) {
        SerializedPerson person = null;

        try(FileInputStream fis = new FileInputStream("Serial.data");
            ObjectInputStream ois = new ObjectInputStream(fis)
        ) {
            person = (SerializedPerson) ois.readObject();
            System.out.println(person);
            System.out.println(person.getFullName());
            System.out.println("Person id=" + person.getId());
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku");
        } catch (IOException e) {
            System.out.println("Błąd wejścia / wyjścia");
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            System.out.println("Nie znaleziono klasy obiektu");
        }
    }
}
