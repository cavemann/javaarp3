package com.sda.io.serialization;

import java.io.Serializable;

/**
 * Serializable jest interfejsem - znacnikiem
 * czyli nie wymaga implementacji żadnej metody
 * tylko powiadamia że dla obiektu dostępna jest
 * jakaś akcja / funkcjonalność - w tym wypadku
 * jest to serializacja
 */

public class SerializedPerson implements Serializable {

    /**
     * Często jest tak że nie wszystkie pola klasy chcemy przeysłać (serializować)
     * W takim przypadku pole, które chcemy pominąć oznaczamy słówkiem transiet
     * Przy deserializacji pole takie zostanie zainicjowane wartością domyślną
     * dla danego typu zmiennej
     * ************************
     * Analogicznie jest przy:
     *  - zamianie obiektu na XML i spowrotem (np pakietem jackson - tam oznacza się anotacją)
     *  - zamianie obiektu na JSON i spowrotem (zdaje się że anotają)
     */

    private transient int id;
    private String name;
    private String surname;
    private int age;

    public SerializedPerson(int id, String name, String surname, int age) {
        this.id = id;
        this.name = name;
        this.surname = surname;
        this.age = age;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getFullName() {
        return surname + " " + name;
    }

    public boolean isAdult() {
        return age >= 18;
    }

    @Override
    public String toString() {
        return "SerializedPerson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", age=" + age +
                '}';
    }
}
