package com.sda.io.simple;

import java.io.*;
import java.util.Scanner;

public class ReadWriteFile {
    public static void main(String[] args) {
        String firstFile = "FirstFile.txt";
        String secondFile = "SecondFile.txt";
        ReadWriteFile readWriteFile = new ReadWriteFile();
        //readWriteFile.simpleWriteToFile(firstFile);
        //readWriteFile.simpleReadFromFile(firstFile);

        readWriteFile.writeFileWithBuffer(secondFile);
        readWriteFile.readFileWithBuffer(secondFile);
    }

    /**
     * SimpleWriteToFile zrobione z pomocą try-with-resources
     * czyli try(zasób - np FileReader) {
     *     operacje
     * } catch ...
     * nie wymaga wywoływania close() na zasobach bo Java zrobi to automagicznie
     * ale zasoby muszą implementować interfejs Closeable albo AutoCloseable
     */
    public void simpleWriteToFile(String filename) {
        try (FileWriter fileWriter = new FileWriter(filename)) {
            System.out.println("Zapisujemy do pliku" + filename);
            fileWriter.write("aaa\n");
            fileWriter.write("bbb\n");
            fileWriter.write("ccc\n");
        } catch (IOException e) {
            System.out.println("Błąd przy zapisie");
        }
        System.out.println("Koniec zapisu");
    }

    /**
     * Try "zwykłe" gdzie po otwarciu zasobów sami musimy zadbać
     * o ich zamknięcie metodą close()
     *
     * więcej tutaj https://java-arp.pl.sdacademy.pro/e-podrecznik/java_podstawy/wyjatki/
     */
    public void simpleReadFromFile(String filename) {
        try {
            FileReader fileReader = new FileReader(filename);
            Scanner scanner = new Scanner(fileReader);
            scanner.useDelimiter("\n");
            System.out.println("Reading from file");
            while (scanner.hasNextLine()) {
                System.out.println(scanner.nextLine());
            }
            scanner.close();
            fileReader.close();
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku " + filename);
        } catch (IOException e) {
            System.out.println("Problem z zamknięciem połączenia do pliku");
        }
        System.out.println("Koniec odczytu z pliku");
    }

    /**
     * Powyżej realizacja odczytu / zapisu znak po znaku
     * Poniżej z pośrednictwem bufora, jak bufor jest pełny, to następuje odczyt / zapis
     * i kolejna iteracja poakowania danych do bufora aż do wyczerpania danych do zapisu / odczytu
     */
    public void writeFileWithBuffer(String filename) {
        try {
            System.out.println("Początek zapisu buforowanego");
            BufferedWriter writer = new BufferedWriter(new FileWriter(filename));
            writer.write("Linia 1\n");
            writer.write("Linia 2\n");
            writer.write("Linia 3\n");
            writer.close();
        } catch (IOException e) {
            System.out.println("Problem z podłączeniem do pliku " + filename);
        }
    }

    public void readFileWithBuffer(String filename) {
        String input;
        System.out.println("Zaczynamy odczyt z pliku " + filename);
        try (BufferedReader reader = new BufferedReader(new FileReader(filename))) {
            while((input = reader.readLine()) != null) {
                System.out.println(input);
            }
        } catch (FileNotFoundException e) {
            System.out.println("Nie znaleziono pliku " + filename);
        } catch (IOException e) {
            System.out.println("Problem z odczytem z pliku " + filename);
        }
        System.out.println("Koniec odczytu z pliku " + filename);
    }
}
