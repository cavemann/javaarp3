package com.sda.io.nio;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.List;

public class ReadWriteNIO {

    /**
     * NIO używa wielowątkowości. Odczyt / zapis nie blokują przebiegu programu, co w przypadku
     * klasycznych FileWriterów i FileReaderów mogło być problemem, w szczególności
     * przy dużych plikach
     */

    public static void main(String[] args) {
        ReadWriteNIO nio = new ReadWriteNIO();
        String filename = "NIOFile.txt";
        try {
            nio.simpleWriteToFileNIO(filename);
            nio.simpleReadFromFileNIO(filename);
        } catch (IOException e) {
            System.out.println("Wystąpił błąd");
            e.printStackTrace();
        }
    }

    public void simpleWriteToFileNIO(String filename) throws IOException {
        System.out.println("Początek zapisu do pliku " + filename);
        Files.writeString(Paths.get(filename), "Linia 1\n");
        Files.writeString(Paths.get(filename), "Linia 2\n", StandardOpenOption.APPEND);
        Files.writeString(Paths.get(filename), "Linia 3\n", StandardOpenOption.APPEND);
        System.out.println("Koniec zapisu do pliku " + filename);
    }

    public void simpleReadFromFileNIO(String filename) throws IOException {
        List<String> fileContent;
        System.out.println("Odczytujemy dane z pliku " + filename);
        fileContent = Files.readAllLines(Paths.get(filename));
        fileContent.forEach(System.out::println);
        System.out.println("Koniec odczytu z pliku " + filename);
    }
}
